﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalisationManager : Singleton<LocalisationManager>
{
	[SerializeField]
	protected bool frenchOverride = false;

	[SerializeField]
	protected TextAsset translationCSV;

	private string[] bounceEnglish, bounceFrench;
	private Dictionary<Lang, string> english, french;

	public bool IsFrench{ get { return Application.systemLanguage == SystemLanguage.French || frenchOverride; } }

	protected override void Setup ()
	{
		if (translationCSV == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		ProcessTranslationCSV ();

		bounceEnglish = new string[] {
			"Supercomputing!",
			"Real wind data!",
			"Our systems generate 25TB of data each day.",
			"Our systems process 100 million environmental observations each day.",
			"We are investigating how to use AI, ML, and GPUs in our prediction systems.",
			"Cloud computing... supercomputing... super cloud computing?",
			"Linux and scientific computing at the MSC.",
			"More than 200 scientists and programmers in Dorval, QC."
		};

		bounceFrench = new string[] {
			"La superinformatique!",
			"Données de vent réel!",
			"Nos systèmes génère 25TB de données par jour.",
			"Nous traitons 100 millions d'observations environnementales par jour.",
			"Nous étudions comment utiliser IA, AM et les GPG dans nos systèmes de prévision.",
			"Infonuagique... superinformatique... superinfonuagique?",
			"Linux et le calcul scientifique au SMC.",
			"Plus de 200 scientifiques et informaticiens à Dorval, QC."
		};
	}

	public string GetTranslation (Lang key)
	{
		SetupIfSetupHasNotRun ();

		var dictionary = IsFrench ? french : english;

		if (!dictionary.ContainsKey (key)) {
			Debug.LogError ("no translation for key: " + key);
			return "";
		}
		return dictionary [key];
	}

	public string GetRandomBounceText ()
	{
		SetupIfSetupHasNotRun ();
		
		string[] array = IsFrench ? bounceFrench : bounceEnglish;
		string choice = array [Random.Range (0, array.Length - 1)];
		return choice;
	}

	private void ProcessTranslationCSV ()
	{
		english = new Dictionary<Lang, string> ();
		french = new Dictionary<Lang, string> ();

		string[,] data = CSVReader.SplitCsvGrid (translationCSV.text, '\t');
		for (int row = 0; row < data.GetLength (1); row++) {
			
			string key = data [0, row];
			string e = data [1, row];
			string f = data [2, row];

			if (key == null || e == null || f == null)
				continue;

			if (key.Length == 0)
				key = e.Replace (" ", "");
				
			Lang keyEnum = (Lang)System.Enum.Parse (typeof(Lang), key);

			english.Add (keyEnum, e);
			french.Add (keyEnum, f);
		}
	}
}

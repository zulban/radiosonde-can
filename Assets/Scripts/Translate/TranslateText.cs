﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class TranslateText : MonoBehaviourWithSetup
{
	[SerializeField]
	protected Lang key;

	protected override void Setup ()
	{
		Text text = GetComponent<Text> ();
		text.text = LocalisationManager.Instance.GetTranslation (key);
	}
}

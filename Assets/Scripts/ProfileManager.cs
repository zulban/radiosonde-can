﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This manages all persistent player data: options, player stats, adventure progress.
/// </summary>
public class ProfileManager : Singleton<ProfileManager>
{
	[SerializeField]
	protected MenuSettings defaultMenuSettings;

	[SerializeField]
	protected ImageCheckbox soundCheckbox;

	// The keys used in PlayerPref save/load lookups
	private const string menuSettingsKey = "menu settings";

	protected override void Setup ()
	{
		if (soundCheckbox == null) {
			Debug.LogError (this + " has null values.");
			return;
		}

		// Save the menu settings whenever a checkbox value is changed.
		ImageCheckbox[] checkboxes = new ImageCheckbox[] {
			soundCheckbox
		};
		foreach (var checkbox in checkboxes)
			checkbox.OnValueChange += SaveMenuSettings;

		MenuSettings menuSettings = PlayerPrefSerialization.LoadObject<MenuSettings> (menuSettingsKey);
		if (menuSettings == null) {
			menuSettings = defaultMenuSettings;
		}

		SetUIWithMenuSettings (menuSettings);
	}

	public void SaveMenuSettings ()
	{
		var settings = GetMenuSettingsFromUI ();
		PlayerPrefSerialization.SaveObject<MenuSettings> (settings, menuSettingsKey);
		PlayManager.Instance.NotifyOptionsChange ();
	}

	public MenuSettings GetMenuSettingsFromUI ()
	{
		var menuSettings = new MenuSettings ();
		menuSettings.sound = soundCheckbox.GetValue ();
		return menuSettings;
	}

	private void SetUIWithMenuSettings (MenuSettings menuSettings)
	{
		soundCheckbox.SetValue (menuSettings.sound, true, false);
	}
}

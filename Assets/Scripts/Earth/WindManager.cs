﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class WindManager : Singleton<WindManager>
{
    [SerializeField]
    protected Canvas arrowsCanvas;

    [SerializeField]
    protected GameObject arrowPrefab;

    [SerializeField]
    public Vector3 minArrowScale = new Vector3(0.003f, 0.010f, 1);

    [SerializeField]
    public Vector3 maxArrowScale = new Vector3(0.01f, 0.03f, 1);

    [SerializeField]
    protected TextAsset windVectorCSV;

    private GameObject earthGO;
    private WindPoint[,] points;
    private GameObject windArrowsContainer;

    private const float latStep = 2.5f;
    private const float lonStep = 2.5f;
    private int latCount, lonCount;
    private float maxWindMagnitude;

    /// <summary>
    /// Easily iterate through all possible lat/lon combinations without copying the same double for loop.
    /// Corresponds to the (i,j) of the internal arrays.
    /// </summary>
    private List<Point2Int> ijPoints;

    private const float epsilon = 0.01f;

    /// <summary>
    /// For a balloon at this position, raycasts to Earth's surface to retrieve the nearest 3D wind vector.
    /// </summary>
    public Vector3 GetWindVector(Vector3 position, bool highlightArrow = false)
    {
        SetupIfSetupHasNotRun();

        Vector2 lonLat = Earth.Instance.GetLonLatFromPosition(position);
        Vector2Int ij = GetArrayIndexesFromLatLon(lonLat.y, lonLat.x);
        var point = points[ij.x, ij.y];

        if (highlightArrow || DebugManager.Instance.HighlightWindArrow)
            HighlightArrow(point);

        return point.wind3D;
    }

    private void HighlightArrow(WindPoint windPoint)
    {
        if (windPoint == null || windPoint.ui == null)
            return;

        DebugManager.Instance.SetArrowTrackingCube(windPoint);
    }

    protected override void Setup()
    {
        if (arrowsCanvas == null || arrowPrefab == null)
        {
            Debug.LogError(this + " has nulls.");
            return;
        }

        latCount = Mathf.FloorToInt(180 / latStep) + 1;
        lonCount = Mathf.FloorToInt(360 / lonStep) + 1;
        points = new WindPoint[lonCount, latCount];
        ijPoints = new List<Point2Int>();
        for (int j = 0; j < latCount; j++)
        {
            for (int i = 0; i < lonCount; i++)
            {
                ijPoints.Add(new Point2Int(i, j));
                points[i, j] = new WindPoint();
            }
        }

        earthGO = Earth.Instance.gameObject;

        string containerName = "Wind Arrows Container";
        windArrowsContainer = GameObject.Find(containerName);
        if (windArrowsContainer == null)
            windArrowsContainer = new GameObject(containerName);
        windArrowsContainer.transform.parent = arrowsCanvas.transform;
        windArrowsContainer.transform.position = Vector3.zero;

        SetupWindPoints();

        if (windVectorCSV == null)
        {
            Debug.Log("No wind vector CSV. Using random values.");
            SetRandomWind();
            //SetWestWind ();
        }
        else
        {
            SetWindWithCSV();
        }
        Debug.Log("maxWindMagnitude = " + maxWindMagnitude);
    }

    private void SetWindWithCSV()
    {
        float[,] csv = CSVReader.ParseFloatCSV(windVectorCSV.text);

        for (int row = 0; row < csv.GetLength(1); row++)
        {
            float lat = csv[0, row];
            float lon = csv[1, row];
            float uwind = csv[2, row];
            float vwind = csv[3, row];
            Vector2 wind = new Vector2(uwind, vwind);
            Vector2Int ij = GetArrayIndexesFromLatLon(lat, lon);
            SetWind(ij.x, ij.y, wind);
        }
    }

    private Vector2Int GetArrayIndexesFromLatLon(float lat, float lon)
    {
        int i = Mathf.FloorToInt((lon + 180) / lonStep);
        int j = Mathf.FloorToInt((lat + 90) / latStep);
        return new Vector2Int(i, j);
    }

    /// <summary>
    /// (i,j) is internal array index
    /// </summary>
    private void SetWind(int i, int j, Vector2 wind2D)
    {
        var point = points[i, j];
        point.wind2D = wind2D;

        Vector3 earthNormal = point.position - earthGO.transform.position;
        Vector3 east = Vector3.Cross(earthNormal, Vector3.up).normalized;
        Vector3 north = Vector3.Cross(east, earthNormal).normalized;

        if (east.sqrMagnitude > epsilon)
        {
            point.wind3D = east * point.wind2D.x + north * point.wind2D.y;
            point.wind3D = point.wind3D.normalized * point.wind2D.magnitude;
        }
        else
        {
            // The poles simply have a left wind. No east or north on poles.
            point.wind3D = Vector3.left;
        }

        maxWindMagnitude = Mathf.Max(maxWindMagnitude, point.wind2D.magnitude);

        RefreshArrowAppearance(i, j);
    }

    /// <summary>
    /// Set all wind vectors to north.
    /// </summary>
    private void SetNorthWind()
    {
        SetAllWind(Mathf.PI / 2);
    }

    private void SetEastWind()
    {
        SetAllWind(Mathf.PI);
    }

    private void SetWestWind()
    {
        SetAllWind(0);
    }

    private void SetAllWind(float radians)
    {
        float magnitude = 1f;
        foreach (var point in ijPoints)
        {
            Vector2 wind2D = magnitude * new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
            SetWind(point.x, point.y, wind2D);
        }
    }

    /// <summary>
    /// Set all wind vectors randomly.
    /// </summary>
    private void SetRandomWind(int seed = 0)
    {
        System.Random r = new System.Random(seed);

        foreach (var point in ijPoints)
        {
            float radians = (float)r.NextDouble() * Mathf.PI * 2;
            float degrees = 360 * radians / (2 * Mathf.PI);
            float magnitude = (float)r.NextDouble() * 2 + 0.1f;
            Vector2 wind2D = magnitude * new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
            SetWind(point.x, point.y, wind2D);
        }
    }

    private void RefreshAllArrowAppearance()
    {
        foreach (var point in ijPoints)
        {
            RefreshArrowAppearance(point.x, point.y);
        }
    }

    private void RefreshArrowAppearance(int i, int j)
    {
        var point = points[i, j];
        WindArrowUI windArrow = point.ui;
        if (windArrow == null)
            return;

        var wind2D = point.wind2D;
        var wind3D = point.wind3D;

        Vector3 earthNormal = point.position - earthGO.transform.position;
        Vector3 east = Vector3.Cross(earthNormal, Vector3.up);
        Vector3 north = Vector3.Cross(east, earthNormal);
        float degrees = Vector3.Angle(wind3D, north);

        // Degrees will always be <= 180 because it's degrees in either direction relative to (0,1)
        // However it should be <= 360
        // So if it's on the right side, we adjust.
        if (wind2D.x > 0)
            degrees = 360 - degrees;

        float magnitude = wind3D.magnitude;

        windArrow.SetAppearance(magnitude, degrees, maxWindMagnitude);
    }

    public Vector3 GetEarthSurfacePositionFromLatLon(float latitudeDegrees, float longitudeDegrees)
    {
        SetupIfSetupHasNotRun();

        float latitudeRadians = latitudeDegrees * (Mathf.PI / 180);
        float longitudeRadians = longitudeDegrees * (Mathf.PI / 180);

        float x = Mathf.Cos(latitudeRadians) * Mathf.Cos(longitudeRadians);
        float y = Mathf.Cos(latitudeRadians) * Mathf.Sin(longitudeRadians);
        float z = Mathf.Sin(latitudeRadians);

        // xzy not xyz to correspond to unity coordinates
        Vector3 offset = Constants.EarthRadius * new Vector3(x, z, y);
        Vector3 position = earthGO.transform.position + offset;

        return position;
    }

    private void SetupWindPoints()
    {
        for (int j = 0; j < latCount; j++)
        {
            int latSkipCount = 0;

            for (int i = 0; i < lonCount; i++)
            {
                float latitudeDegrees = (j * latStep - 90);
                float longitudeDegrees = (i * lonStep - 180);

                Vector3 position = GetEarthSurfacePositionFromLatLon(latitudeDegrees, longitudeDegrees);

                var point = points[i, j];
                point.position = position;

                // Skip making some visual arrows. Too many arrows near poles.
                int latSkip = Mathf.Max(0, Mathf.FloorToInt((Mathf.Abs(latitudeDegrees) - 60) / 5f));
                bool shouldCreateArrowUI = true;
                if (latSkip > 0)
                {
                    latSkipCount++;
                    if (latSkipCount < latSkip)
                    {
                        shouldCreateArrowUI = false;
                    }
                    else
                    {
                        latSkipCount = 0;
                    }
                }

                if (shouldCreateArrowUI)
                {
                    Quaternion rotation = Quaternion.identity;
                    GameObject arrow = Instantiate<GameObject>(arrowPrefab, position, rotation, windArrowsContainer.transform);
                    arrow.name = "Arrow (" + i + ", " + j + ") lon/lat=(" + longitudeDegrees + ", " + latitudeDegrees + ")";

                    // align the arrows to the earth tangent
                    arrow.transform.LookAt(earthGO.transform);

                    // tint longitude zero, latitude zero
                    if (Mathf.Abs(latitudeDegrees) < epsilon)
                        arrow.GetComponent<Image>().color = Color.red;
                    if (Mathf.Abs(longitudeDegrees) < epsilon || Mathf.Abs(longitudeDegrees) > 180 - epsilon)
                        arrow.GetComponent<Image>().color = Color.green;

                    point.ui = new WindArrowUI(arrow);
                    point.arrowImage = point.ui.arrowGO.GetComponent<Image>();
                }
            }
        }
    }
}

public class WindPoint
{
    public WindArrowUI ui;
    public Image arrowImage;
    public Vector3 position;
    public Vector2 wind2D;
    public Vector3 wind3D;
}

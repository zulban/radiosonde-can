﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthTarget : Singleton<EarthTarget>
{
	[SerializeField]
	protected Vector2 startLatLon = Constants.MontrealLatLon;

	private Vector2 minMaxScale = new Vector2 (0.7f, 1);

	void Start ()
	{
		SetLatLon (startLatLon.x, startLatLon.y);
	}

	public void SetLatLon (float latitudeDegrees, float longitudeDegrees)
	{
		Vector3 position = WindManager.Instance.GetEarthSurfacePositionFromLatLon (latitudeDegrees, longitudeDegrees);
		transform.position = position;
		transform.LookAt (position * 2);
	}

	void Update ()
	{
		float r1 = (1 + Mathf.Sin (Time.timeSinceLevelLoad * 1.13f)) / 2;
		float r2 = (1 + Mathf.Sin (Time.timeSinceLevelLoad * 1.37f)) / 2;
		transform.localScale = new Vector3 (Mathf.Lerp (minMaxScale.x, minMaxScale.y, r1),
			Mathf.Lerp (minMaxScale.x, minMaxScale.y, r2), 1);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshUtilities : MonoBehaviour
{
	void Start ()
	{
		SplitMesh (gameObject, transform);
	}

	/// <summary>
	/// meshToSet becomes a mesh with just one triangle, taken from meshWithTriangles, using triangleIndex to choose the triangle.
	/// </summary>
	private static void SetMeshWithMeshTriangle (Mesh meshToSet, Mesh meshWithTriangles, Vector3 offset, int triangleIndex)
	{
		int[] vertexIndexes = {meshWithTriangles.triangles [triangleIndex],
			meshWithTriangles.triangles [triangleIndex + 1],
			meshWithTriangles.triangles [triangleIndex + 2]
		};
		meshToSet.vertices = new Vector3[] {meshWithTriangles.vertices [vertexIndexes [0]] + offset,
			meshWithTriangles.vertices [vertexIndexes [1]] + offset,
			meshWithTriangles.vertices [vertexIndexes [2]] + offset
		};
		meshToSet.uv = new Vector2[] {meshWithTriangles.uv [vertexIndexes [0]],
			meshWithTriangles.uv [vertexIndexes [1]],
			meshWithTriangles.uv [vertexIndexes [2]]
		};
		meshToSet.normals = new Vector3[] {meshWithTriangles.normals [vertexIndexes [0]],
			meshWithTriangles.normals [vertexIndexes [1]],
			meshWithTriangles.normals [vertexIndexes [2]]
		};

		meshToSet.triangles = new int[]{ 0, 1, 2 };
	}

	private static Vector3 GetCenterOfMesh (Mesh mesh)
	{
		if (mesh.vertexCount == 0) {			
			Debug.LogError ("GetCenterOfMesh failed, no vertices.");
			return Vector3.zero;
		}
		Vector3 sum = Vector3.zero;
		for (int i = 0; i < mesh.vertexCount; i++)
			sum += mesh.vertices [i];
		return sum / mesh.vertexCount;
	}

	private static Vector3 GetCenterOfTriangleInMesh (Mesh mesh, int triangleIndex)
	{
		Vector3[] vertices = {mesh.vertices [mesh.triangles [triangleIndex]],
			mesh.vertices [mesh.triangles [triangleIndex + 1]],
			mesh.vertices [mesh.triangles [triangleIndex + 2]]
		};
		return (vertices [0] + vertices [1] + vertices [2]) / 3;
	}

	public static GameObject[] SplitMesh (GameObject toSplit, Transform container)
	{
		toSplit.GetComponent<MeshRenderer> ().enabled = false;

		Mesh originalMesh = toSplit.GetComponent<MeshFilter> ().mesh;
		Material newMaterial = new Material (toSplit.GetComponent<MeshRenderer> ().material);
		MeshAnalyzer meshAnalyzer = new MeshAnalyzer (originalMesh);

		List<GameObject> pieces = new List<GameObject> ();
		for (int triangleIndex = 0; triangleIndex < originalMesh.triangles.Length; triangleIndex +=3) {
			GameObject piece = new GameObject ("piece" + triangleIndex);
			Vector3 offset = GetCenterOfTriangleInMesh (originalMesh, triangleIndex);
			piece.transform.position = offset;
			piece.transform.parent = container;

			piece.AddComponent<MeshFilter> ();
			piece.AddComponent<MeshRenderer> ();
			piece.GetComponent<MeshRenderer> ().material = newMaterial;
			Mesh newMesh = piece.GetComponent<MeshFilter> ().mesh;

			SetMeshWithMeshTriangle (newMesh, originalMesh, -offset, triangleIndex);

			pieces.Add (piece);
		}

		return pieces.ToArray();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LayerLookup {
	public static int GetLayerFromName(string layerName){
		int layer = LayerMask.NameToLayer(layerName);
		if(layer==-1)
			Debug.LogError("LayerLookup failed to find layer for '"+layerName+"'");
		return layer;
	}
}

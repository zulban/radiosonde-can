﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindArrowUI
{
	public WindArrowUI (GameObject arrow)
	{
		this.arrowGO = arrow;
	}

	public GameObject arrowGO;

	public void SetAppearance (float magnitude, float degrees, float maxWindMagnitude)
	{
		if (Mathf.Abs (magnitude) > maxWindMagnitude + 0.01f) {
			Debug.LogError (this + " magnitude " + magnitude + " greater than maxMagnitude = " + maxWindMagnitude);
			magnitude = Mathf.Clamp (magnitude, -maxWindMagnitude, maxWindMagnitude);
		}
		float normalizedMagnitude = magnitude / maxWindMagnitude;
		arrowGO.transform.localScale = Vector3.Lerp (WindManager.Instance.minArrowScale, WindManager.Instance.maxArrowScale, normalizedMagnitude);

		Vector3 r = arrowGO.transform.eulerAngles;
		r.z = degrees;
		arrowGO.transform.eulerAngles = r;
	}
}
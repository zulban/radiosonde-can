﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshAnalyzer
{
	private Mesh mesh;

	private PointOctree<int> octree;

	private const float minNodeSize = 0.0001f;
	private const float duplicateThreshold = minNodeSize / 10;

	/// <summary>
	/// Key is a triangle0Index, value is triangle0Index of edge neighbours
	/// </summary>
	private Dictionary<int,List<int>> triangleEdgeNeighbours;

	public MeshAnalyzer (Mesh mesh)
	{
		this.mesh = mesh;

		octree = new PointOctree<int> (1, Vector3.zero, minNodeSize);
		for (int i = 0; i < mesh.vertexCount; i++) {
			octree.Add (i, mesh.vertices [i]);
		}

		// pair up vertices to triangles
		// Key is a representative vertex index, values are every triangle that uses that vertex.
		Dictionary<int,List<int>> repVertexTriangles = new Dictionary<int, List<int>> ();

		for (int triangle0Index = 0; triangle0Index < mesh.triangles.Length; triangle0Index += 3) {
			for (int i = 0; i < 3; i++) {
				int vertex = mesh.triangles [triangle0Index + i];
				int repVertex = GetRepresentativeVertex (vertex);
				if (!repVertexTriangles.ContainsKey (repVertex))
					repVertexTriangles.Add (repVertex, new List<int> ());
				repVertexTriangles [repVertex].Add (triangle0Index);
			}
		}

		//build the dictionary that pairs triangles to other triangles
		triangleEdgeNeighbours = new Dictionary<int, List<int>> ();
		for (int triangle0Index = 0; triangle0Index < mesh.triangles.Length; triangle0Index += 3) {
			HashSet<int> vertexNeighbours = new HashSet<int> (); //values are triangle0Index of vertex neighbours

			for (int i = 0; i < 3; i++) {
				int repVertex = GetRepresentativeVertex (mesh.triangles [triangle0Index+i]);
				foreach (int neighbourTriangle0Index in repVertexTriangles[repVertex]) {
					if (neighbourTriangle0Index == triangle0Index)
						continue;
					if (vertexNeighbours.Contains (neighbourTriangle0Index)) {
						if (!triangleEdgeNeighbours.ContainsKey (triangle0Index))
							triangleEdgeNeighbours.Add (triangle0Index, new List<int> ());
						triangleEdgeNeighbours [triangle0Index].Add (neighbourTriangle0Index);
					} else {
						vertexNeighbours.Add (neighbourTriangle0Index);
					}
				}
			}
		}

		//DebugReport ();
	}

	public int[] GetTriangleEdgeNeighbours (int triangle0Index)
	{
		if (triangleEdgeNeighbours.ContainsKey (triangle0Index)) {
			return triangleEdgeNeighbours [triangle0Index].ToArray ();
		} else {
			Debug.LogError ("GetTriangleEdgeNeighbours, not given a valid triangle0Index = " + triangle0Index);
			Debug.Log ("triangleEdgeNeighbours keys.length = " + triangleEdgeNeighbours.Keys.Count);
			return new int[0];
		}			
	}

	/// <summary>
	/// If this vertex has duplicates, returns the lowest dulpicate vertex index.
	/// This helps to use vertex indexes in lookup tables.
	/// </summary>
	public int GetRepresentativeVertex (int vertex)
	{
		return Mathf.Min (GetVerticesEquivalentToVertex (vertex));
	}

	public int[] GetVerticesEquivalentToVertex (int vertex)
	{
		return octree.GetNearby (mesh.vertices [vertex], duplicateThreshold);
	}

	public bool IsDuplicateVertex (int vertex)
	{
		int[] nearby = octree.GetNearby (mesh.vertices [vertex], duplicateThreshold);
		return nearby.Length > 1;
	}

	public int CountDuplicateVertices(){
		int count = 0;
		for (int i = 0; i < mesh.vertices.Length; i++)
			if (IsDuplicateVertex (i))
				count++;
		return count;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureManager : Singleton<GestureManager>
{
	public delegate void TapEventHandler (Vector2 a);

	public delegate void SpinEventHandler (Vector2 a, Vector2 b);

	public delegate void ZoomEventHandler (int[] cell, float cellPixelWidth, int fingerId);

	public delegate void NoArgsEventHandler ();

	public event TapEventHandler OnTap;
	public event SpinEventHandler OnSpin;
	public event NoArgsEventHandler OnStartZoom;
	public event ZoomEventHandler OnZoom;
	public event NoArgsEventHandler OnEndZoom;

	[SerializeField]
	[Tooltip ("A gesture that lasts longer than this is not a tap, it's a drag.")]
	private float tapSecondsThreshold = 0.3f;

	[SerializeField]
	protected Camera gestureCamera;

	[SerializeField]
	protected Vector2 minMaxCameraDistance = new Vector2 (Constants.EarthRadius + 5, Constants.EarthRadius + 20);

	[SerializeField]
	protected float zoomSpeed = 1f;

	[SerializeField]
	protected float startZoom = 0f;

	[SerializeField]
	protected float touchZoomFactor = 0.002f;

	private float currentZoom = 0;

	public Camera GestureCamera{ get { return gestureCamera; } }

	private float cellPixelWidth;

	public float CellPixelWidth{ get { return cellPixelWidth; } }

	private bool useTouch = true;

	private TouchInfo[] touchInfos = new TouchInfo[2];

	private Vector2 mousePositionLastFrame;

	protected override void Setup ()
	{
		currentZoom = startZoom;

		if (SystemInfo.deviceType == DeviceType.Desktop)
			useTouch = false;

		if (gestureCamera == null) {
			Debug.LogWarning (this + " has null camera. Setting to main.");
			gestureCamera = Camera.main;
		}
	}

	private void UpdateTouch ()
	{
		if (Input.touchCount == 1) {
			Touch touch = Input.touches [0];
			if (touch.phase == TouchPhase.Began) {
				touchInfos [0] = new TouchInfo (touch.position);
			} else if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) {
				float duration = Time.timeSinceLevelLoad - touchInfos [0].startTime;
				if (duration < tapSecondsThreshold) {
					TapGesture (touch.position);
				}
			} else if (touch.phase == TouchPhase.Moved) {
				Vector2 previousPosition = touch.position - touch.deltaPosition;
				SpinGesture (previousPosition, touch.position);
			}

		} else if (Input.touchCount == 2) {
			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Began)
					StartZoomGesture ();

				ZoomGesture (touch.position, touch.fingerId);

				if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
					EndZoomGesture ();
			}
		}
	}

	private void DetectZoom ()
	{		
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			//ScrollWheel is typically +/- 0.05 and within [-0.25, 0.25]
			ChangeCurrentZoom (Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed);
		}

		if (Input.touchCount == 2) {
			// Store both touches.
			Touch touch0 = Input.GetTouch (0);
			Touch touch1 = Input.GetTouch (1);

			// Find the position in the previous frame of each touch.
			Vector2 touch0PreviousPosition = touch0.position - touch0.deltaPosition;
			Vector2 touch1PreviousPosition = touch1.position - touch1.deltaPosition;

			// Find the magnitude of the vector between the touches in each frame.
			float prevTouchDeltaMag = (touch0PreviousPosition - touch1PreviousPosition).magnitude;
			float touchDeltaMag = (touch0.position - touch1.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;

			ChangeCurrentZoom (deltaMagnitudeDiff * zoomSpeed * touchZoomFactor);
		}
	}

	private void ChangeCurrentZoom (float delta)
	{		
		float newZoom = currentZoom + delta;
		newZoom = Mathf.Clamp (newZoom, 0, 1);
		SetZoom (newZoom);
	}

	private void SetZoom (float newZoom)
	{
		currentZoom = newZoom;
		Vector3 unitCamera = (gestureCamera.gameObject.transform.position - Earth.Instance.gameObject.transform.position).normalized;
		float magnitude = Mathf.Lerp (minMaxCameraDistance.y, minMaxCameraDistance.x, newZoom);
		gestureCamera.transform.position = unitCamera * magnitude;
	}

	private void UpdateMouse ()
	{
		if (Input.GetKey (KeyCode.Z)) {
			if (Input.GetMouseButtonDown (0)) {
				int fingerId = 1;
				StartZoomGesture ();
				ZoomGesture (Input.mousePosition, fingerId);
			}
			if (Input.GetMouseButton (0)) {
				int fingerId = 0;
				ZoomGesture (Input.mousePosition, fingerId);
			}
			if (Input.GetMouseButtonUp (0)) {
				EndZoomGesture ();
			}
		} else {			
			if (Input.GetMouseButtonDown (0)) {
				mousePositionLastFrame = Input.mousePosition;
				touchInfos [0] = new TouchInfo (Input.mousePosition);
			} else if (Input.GetMouseButton (0)) {
				SpinGesture (mousePositionLastFrame, Input.mousePosition);
				mousePositionLastFrame = Input.mousePosition;
			} else if (Input.GetMouseButtonUp (0)) {
				float duration = Time.timeSinceLevelLoad - touchInfos [0].startTime;
				if (duration < tapSecondsThreshold)
					TapGesture (Input.mousePosition);
			}
		}
	}

	private void TapGesture (Vector2 point)
	{
		Debug.Log (this + " TapGesture point = " + point);
		if (OnTap != null)
			OnTap (point);
	}

	private void SpinGesture (Vector2 startPoint, Vector2 endPoint)
	{
		if (OnSpin != null)
			OnSpin (startPoint, endPoint);
	}

	private void StartZoomGesture ()
	{
		Debug.Log (this + " StartZoomGesture");
		if (OnStartZoom != null)
			OnStartZoom ();
	}

	private void ZoomGesture (Vector2 point, int fingerId)
	{
		if (OnZoom != null)
			OnZoom (null, cellPixelWidth, fingerId);
	}

	private void EndZoomGesture ()
	{
		Debug.Log ("EndZoomGesture");
		if (OnEndZoom != null)
			OnEndZoom ();
	}

	void Update ()
	{
		DetectZoom ();

		if (useTouch) {
			UpdateTouch ();
		} else {
			UpdateMouse ();
		}
	}

	private class TouchInfo
	{
		public Vector2 startPoint;
		public float startTime;

		public TouchInfo (Vector2 startPoint)
		{
			startTime = Time.timeSinceLevelLoad;
			this.startPoint = startPoint;
		}
	}
}

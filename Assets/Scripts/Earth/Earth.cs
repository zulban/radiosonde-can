﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earth : Singleton<Earth>
{
	protected bool isSetup = false;

	[SerializeField]
	protected Collider gestureCollider;

	[SerializeField]
	[Tooltip ("The camera must be a child of a GameObject which has the Earth center as its center. The camera is offset from this GameObject.")]
	protected Transform cameraContainer;

	private int earthMask, earthLayer;

	public bool CanInteract{ get { return NavigationManager.Instance.CurrentPageType == PageTypes.Play; } }

	/// <summary>
	/// Seconds of inactivity until the Earth starts auto rotating.
	/// </summary>
	private const float autorotateAfterSeconds = 60;

	void Update ()
	{
		if (NavigationManager.Instance.SecondsSinceLastActivity > autorotateAfterSeconds) {
			cameraContainer.transform.RotateAround (gameObject.transform.position, Vector3.up, Time.deltaTime);
		}
	}

	public Vector2 GetLonLatFromPosition (Vector3 position)
	{
		Vector3 equatorProjection = new Vector3 (position.x, 0, position.z);
		float longitude = Vector3.Angle (Vector3.right, equatorProjection);
		float latitude = Vector3.Angle (Vector3.down, position) - 90;

		if (position.z < 0)
			longitude *= -1;
		
		return new Vector2 (longitude, latitude);
	}

	protected override void Setup ()
	{
		if (gestureCollider == null || cameraContainer == null) {
			Debug.LogError (this + " has null values.");
			return;
		}

		//get and apply layers
		earthLayer = LayerLookup.GetLayerFromName ("EarthSkin");
		earthMask = ~earthLayer;

		//register the gesture events
		GestureManager.Instance.OnTap += OnTap;
		GestureManager.Instance.OnSpin += OnSpin;
		GestureManager.Instance.OnStartZoom += OnStartZoom;
		GestureManager.Instance.OnZoom += OnZoom;
		GestureManager.Instance.OnEndZoom += OnEndZoom;
	}

	private Vector3 GetEarthPointFromScreenXY (Vector2 point)
	{
		RaycastHit hit;
		Ray ray = GestureManager.Instance.GestureCamera.ScreenPointToRay (point);
		if (Physics.Raycast (ray, out hit, Mathf.Infinity, earthMask)) {
			if (hit.collider.gameObject.layer == earthLayer)
				return hit.point;
			else
				return Vector3.zero;
		} 

		return Vector3.zero;
	}

	protected void OnTap (Vector2 point2D)
	{
		NavigationManager.Instance.NotifyActivity ();

		if (!CanInteract)
			return;
		
		Vector3 position = GetEarthPointFromScreenXY (point2D);

		// Do nothing if did not tap earth
		if (position.sqrMagnitude == 0)
			return;
		
		PlayManager.Instance.NotifyEarthTap (position);
	}

	protected void OnStartZoom ()
	{
		NavigationManager.Instance.NotifyActivity ();

		if (!CanInteract)
			return;
		
	}

	protected void OnZoom (int[] cell, float cellPixelWidth, int fingerId)
	{
		NavigationManager.Instance.NotifyActivity ();

		if (!CanInteract)
			return;
	}

	protected void OnEndZoom ()
	{
		NavigationManager.Instance.NotifyActivity ();

		if (!CanInteract)
			return;
		
	}

	private void OnSpin (Vector2 startPoint, Vector2 endPoint)
	{				
		NavigationManager.Instance.NotifyActivity ();

		if (!CanInteract)
			return;
		
		Camera c = GestureManager.Instance.GestureCamera;

		Vector3 a, b;

		RaycastHit hit;
		Ray ray = c.ScreenPointToRay (startPoint);
		if (Physics.Raycast (ray, out hit, Mathf.Infinity, earthMask)) {
			a = hit.point;
		} else {
			//spin wasn't started on the fruit
			return;
		}

		ray = c.ScreenPointToRay (endPoint);
		if (Physics.Raycast (ray, out hit, Mathf.Infinity, earthMask)) {
			b = hit.point;
		} else {
			//spin wasn't ended on the fruit
			return;
		}

		Vector3 axis = Vector3.Cross (a, b);
		float angle = -1 * Vector3.Angle (a - transform.position, b - transform.position);
		cameraContainer.RotateAround (transform.position, axis, angle);

		// make the top of the screen always point to north pole
		Vector3 r = cameraContainer.transform.rotation.eulerAngles;
		r.z = 0;
		cameraContainer.transform.rotation = Quaternion.Euler (r);
	}
}

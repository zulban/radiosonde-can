﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
	public const float EarthRadius = 20.5f;
	public const float EarthDiameter = EarthRadius * 2;

	public static Vector2 MontrealLatLon = new Vector2 (45.5017f, -73.5673f);

	public static string Yes{ get { return LocalisationManager.Instance.GetTranslation (Lang.Yes); } }

	public static string No{ get { return LocalisationManager.Instance.GetTranslation (Lang.No); } }
}

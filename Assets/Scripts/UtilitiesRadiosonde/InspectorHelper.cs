﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Add InspectorHelper to different GameObjects to automate human work in the inspector like setting up fields, GameObject links, and values.
/// Add to InspectorHelperEditor to add inspector buttons.
/// These functions change the project and typically you save your changes after running them.
/// </summary>
public class InspectorHelper : MonoBehaviour
{
	public void ShowLatLon ()
	{
		Debug.Log (Earth.Instance.GetLonLatFromPosition (gameObject.transform.position));
	}
}

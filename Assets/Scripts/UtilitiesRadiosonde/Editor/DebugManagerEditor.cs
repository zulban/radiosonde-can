﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(DebugManager))]
public class DebugManagerEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		int width = 300;

		DebugManager dm = (DebugManager)target;
		
		GUILayout.Label ("Run Unit Tests", GUILayout.Width (width));
		if (GUILayout.Button ("Run", GUILayout.Width (width))) {
			dm.RunTests ();
		}
	}
}

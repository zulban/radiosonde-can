﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;

class PrebuildVersionIncrement : IPreprocessBuildWithReport
{
	public int callbackOrder { get { return 0; } }

	public void OnPreprocessBuild (UnityEditor.Build.Reporting.BuildReport buildReport)
	{
		VersionManager.Instance.RefreshVersionText ();
	}
}
#endif

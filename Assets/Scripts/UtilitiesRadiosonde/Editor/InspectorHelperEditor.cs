﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(InspectorHelper))]
public class InspectorHelperEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		int width = 300;

		InspectorHelper helper = (InspectorHelper)target;
        
		GUILayout.Label ("Show lat/lon of this GameObject", GUILayout.Width (width));
		if (GUILayout.Button ("Show", GUILayout.Width (width))) {
			helper.ShowLatLon ();
		}

		GUILayout.Label ("BalloonManager.StartBalloons", GUILayout.Width (width));
		if (GUILayout.Button ("StartBalloons", GUILayout.Width (width))) {
			BalloonManager.Instance.ReplayAllBalloons ();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(NavigationManager))]
public class NavigationManagerEditor : Editor
{
	private PageTypes previousNavigateTo = PageTypes.MainMenu;
	private PageTypes navigateTo = PageTypes.MainMenu;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		int width = 300;

		GUILayout.Label ("\nUse this to quickly navigate to\nUI pages when the game is not running.", GUILayout.Width (width));
		navigateTo = (PageTypes)EditorGUILayout.EnumPopup ("Navigate to:", navigateTo);
		if (previousNavigateTo != navigateTo) {
			previousNavigateTo = navigateTo;
			NavigationManager.Instance.SetPage (navigateTo);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class DebugManager : Singleton<DebugManager>
{
	[SerializeField]
	protected bool isEnabled = false;

	[SerializeField]
	protected bool highlightWindArrow = false;

	[SerializeField]
	protected bool runTests = false;

	[SerializeField]
	protected GameObject debugCubePrefab;

	public bool IsEnabled{ get { return isEnabled; } }

	public bool HighlightWindArrow{ get { return highlightWindArrow; } }

	private GameObject arrowTrackingCube;

	void Start ()
	{
		if (isEnabled && runTests)
			RunTests ();
	}

	protected override void Setup ()
	{
		if (debugCubePrefab == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		arrowTrackingCube = Instantiate<GameObject> (debugCubePrefab);
		arrowTrackingCube.transform.parent = transform;
		arrowTrackingCube.SetActive (false);
	}

	public void SetArrowTrackingCube (WindPoint windPoint)
	{
		SetupIfSetupHasNotRun ();

		arrowTrackingCube.SetActive (true);
		arrowTrackingCube.name = "wind2D = " + windPoint.wind2D + " wind3D = " + windPoint.wind3D;
		arrowTrackingCube.transform.position = windPoint.ui.arrowGO.transform.position;
	}

	public void RunTests ()
	{
		ShowLatLonTest (new Vector3 (1, 0, 0));
		ShowLatLonTest (new Vector3 (0, 1, 0));
		ShowLatLonTest (new Vector3 (-1, 0, 0));
		ShowLatLonTest (new Vector3 (0, 0, 1));
		ShowLatLonTest (new Vector3 (0, 0, -1));
	}

	private void ShowLatLonTest (Vector3 position)
	{
		Vector3 result = Earth.Instance.GetLonLatFromPosition (position);
		Debug.Log ("position = " + position + " lat/lon = " + result);
	}
}

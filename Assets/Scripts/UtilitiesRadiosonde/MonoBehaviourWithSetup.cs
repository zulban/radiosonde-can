﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inherit from this MonoBehaviour to have a very useful setup/start pattern.
/// 
/// MonoBehaviours will sometimes rely on "Start" but in later use, their methods are used before Start has a chance to run.
/// Simply spam SetupIfSetupHasNotRun in functions wherever necessary.
/// </summary>
public abstract class MonoBehaviourWithSetup : MonoBehaviour
{
	private bool hasRunSetup = false;

	protected virtual void Start ()
	{
		SetupIfSetupHasNotRun ();
	}

	public void SetupIfSetupHasNotRun ()
	{
		if (!hasRunSetup) {
			hasRunSetup = true;
			Setup ();
		}
	}

	protected abstract void Setup ();
}

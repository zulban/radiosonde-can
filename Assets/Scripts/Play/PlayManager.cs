﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Threading;

/// <summary>
/// This class connects PlaySettings with one Board to the play screen UI.
/// </summary>
public class PlayManager : Singleton<PlayManager>
{
	[SerializeField]
	protected Image inflateBalloon, inflateOutline;

	[SerializeField]
	protected Text scoreTotal, splashScoreText, scoreFive;

	[SerializeField]
	protected Image menuButtonImage;

	private GameState gameState;
	private PlayStatus playStatus;

	private const int scoreCount = 5;
	private List<int> scores;

	private bool isSplashingScore = false;
	private float splashSeconds;
	private const float splashDuration = 1;
	private Vector2 splashMinMaxScale = new Vector2 (0.5f, 1);
	private Vector3 splashStartPosition, splashEndPosition;

	private Color playerColor = Color.red;

	public Color PlayerColor{ get { return playerColor; } }

	private Color[] playerColors;

	void Update ()
	{
		if (isSplashingScore) {
			splashSeconds += Time.deltaTime;
			if (splashSeconds > splashDuration) {
				isSplashingScore = false;
				splashScoreText.gameObject.SetActive (false);
			} else {
				float ratio = Mathf.Pow (1 - Mathf.Clamp (splashSeconds / splashDuration, 0, 1), 2);
				float r = Mathf.Lerp (splashMinMaxScale.x, splashMinMaxScale.y, 1 - ratio);
				Vector3 scale = new Vector3 (r, r, r);
				splashScoreText.gameObject.transform.localScale = scale;

				r = splashSeconds / splashDuration;
				r = Mathf.Pow (r, 20);
				Vector3 newPosition = Vector3.Lerp (splashStartPosition, splashEndPosition, r);
				splashScoreText.gameObject.transform.position = newPosition;
			}
		}
	}

	/// <summary>
	/// When a native mobile "back" or "exit" button is pressed while in play.
	/// </summary>
	public void ClickBack ()
	{
		
	}

	public void NotifyEarthTap (Vector3 position)
	{
		NavigationManager.Instance.NotifyActivity ();

		if (playStatus == PlayStatus.PlaceBalloon) {
			int playerID = 0;
			BalloonManager.Instance.PlaceNewBalloon (position, playerID);
		}
	}

	public void NotifyBalloonLanding (Balloon balloon)
	{
		float distance = Vector3.Distance (EarthTarget.Instance.gameObject.transform.position, balloon.transform.position);
		float ratio = 1.01f - distance / Constants.EarthDiameter;
		int score = Mathf.RoundToInt (1000 * Mathf.Pow (ratio, 4));
		score = Mathf.RoundToInt (score * (1 + balloon.InflateAmount));
		AppendScore (score);
	}

	private void AppendScore (int score)
	{
		SetupIfSetupHasNotRun ();

		// replace a zero with this score
		// if all scores are filled, insert this one if it's bigger
		if (scores.Contains (0)) {
			scores [scores.IndexOf (0)] = score;
		} else {
			for (int i = 0; i < scoreCount; i++) {
				if (i >= scores.Count || scores [i] < score) {
					scores.Insert (i, score);
					break;
				}
			}
		}

		SplashScoreValue (score);
		RefreshScoreUI ();
	}

	/// <summary>
	/// Immediately set scoreboard text values.
	/// </summary>
	private void RefreshScoreUI ()
	{
		string five = "";
		for (int i = 0; i < scoreCount; i++) {
			if (i > 0)
				five += "\n";
			five += (i + 1) + ": ";
			if (scores.Count > i && scores [i] > 0)
				five += scores [i];
		}
		scoreFive.text = five;
		scoreTotal.text = scores.Sum ().ToString ();
	}

	/// <summary>
	/// Start a new splash animation of a score number.
	/// </summary>
	private void SplashScoreValue (int score)
	{
		splashScoreText.text = score.ToString ();
		isSplashingScore = true;
		splashSeconds = 0;
		splashScoreText.gameObject.transform.localScale = Vector3.zero;
		splashScoreText.gameObject.SetActive (true);
	}

	public void ClickMenuIcon ()
	{
		NavigationManager.Instance.NotifyActivity ();

		string[] labels = new string[] {
			LocalisationManager.Instance.GetTranslation (Lang.MainMenu),
			LocalisationManager.Instance.GetTranslation (Lang.ExitApp)
		};
		System.Action[] clickActions = new System.Action[] {
			ClickMainMenu, ClickExit
		};
		PopupManager.Instance.ShowMenuPopup (labels, clickActions);
	}

	public void ClickMainMenu ()
	{
		NavigationManager.Instance.NotifyActivity ();
	}

	public void ClickExit ()
	{
		NavigationManager.Instance.NotifyActivity ();

		NavigationManager.Instance.ClickExit ();
	}

	public void NotifyOptionsChange ()
	{

	}

	public void NewGame ()
	{
		NavigationManager.Instance.NotifyActivity ();

		BalloonManager.Instance.ClearBalloons ();
		scores = new List<int> ();
		RefreshScoreUI ();
		SetRandomColor ();
	}

	private void SetRandomColor ()
	{
		playerColor = playerColors [Random.Range (0, playerColors.Length - 1)];
		RefreshColors ();
	}

	private void RefreshColors ()
	{
		splashScoreText.color = playerColor;
		scoreTotal.color = playerColor;
		scoreFive.color = playerColor;
		menuButtonImage.color = playerColor;
		InflateUI.Instance.SetColor (playerColor);
	}

	protected override void Setup ()
	{			
		if (inflateBalloon == null || inflateOutline == null || splashScoreText == null || scoreFive == null || menuButtonImage == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		playerColors = new Color[] {
			Color.red,
			new Color (150, 0, 0),
			Color.blue,
			new Color (0, 0, 150),
			Color.green,
			new Color (0, 150, 0),
			Color.magenta,
			new Color (150, 0, 150),
			new Color (150, 150, 0),
			new Color (150, 150, 150),
			Color.yellow
		};

		scores = new List<int> ();
		SetRandomColor ();

		splashStartPosition = splashScoreText.gameObject.transform.position;
		splashEndPosition = splashStartPosition + Vector3.down * Screen.height / 2;
		splashScoreText.gameObject.SetActive (false);
		RefreshScoreUI ();
	}
}

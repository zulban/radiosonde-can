using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.Reflection;
using System.Linq;
using System;

public class JSONReader
{
	private string rawData;
	private Dictionary<string,JSONReader> readers;

	public JSONReader (string data)
	{
		JSON.AssertValid (data);
		ParseData (data);
	}

	public string[] GetKeys ()
	{
		return readers.Keys.ToArray ();
	}

	public string GetString (string query)
	{
		string value = GetJsonString (query);

		// Trim quotations
		bool hasQuotes = value.StartsWith ("\"") && value.EndsWith ("\"");
		string result = hasQuotes ? value.Substring (1, value.Length - 2) : value;		

		return result.Replace ("\\", "");
	}

	public int GetInt (string query)
	{
		string value = GetJsonString (query);
		return int.Parse (value);
	}

	public float GetFloat (string query)
	{
		string value = GetJsonString (query);
		return float.Parse (value);
	}

	public bool GetBool (string query)
	{
		string value = GetJsonString (query);
		return value.Equals ("true");
	}

	public DateTime GetDateTime (string query)
	{
		string value = GetJsonString (query);
		int seconds = int.Parse (value);
		return seconds.ConvertUnixTimeStampSeconds ();
	}

	public int[] GetIntArray (string query)
	{
		string value = GetJsonString (query);
		//strip square brackets
		value = value.Substring (1, value.Length - 2);

		string[] split = value.Split (',');
		int[] array = new int[split.Length];
		for (int i = 0; i < split.Length; i++) {
			int number = int.Parse (split [i]);
			array [i] = number;
		}
		return array;
	}

	public bool[] GetBoolArray (string query)
	{
		string value = GetJsonString (query);
		List<bool> booleans = new List<bool> ();
		foreach (char c in value) {
			bool isTrue = c == '1';
			bool isFalse = c == '0';
			if (isTrue)
				booleans.Add (true);
			else if (isFalse)
				booleans.Add (false);
		}
		return booleans.ToArray ();
	}

	public bool[,] Get2DBoolArray (string query)
	{
		string value = GetJsonString (query);

		string[] rows = value.Split ("],");
		int rowCount = rows.Length;
		int columnCount = rows [0].Split (',').Length;
		bool[,] array = new bool[rowCount, columnCount];
		int i = 0;
		int j = 0;
		foreach (char c in value) {
			bool isTrue = c == '1';
			bool isFalse = c == '0';
			if (isTrue || isFalse) {
				array [i, j] = isTrue;
				i++;
				if (i >= rowCount) {
					i = 0;
					j++;
				}
			}
		}
		return array;
	}

	public string GetData ()
	{
		return rawData;
	}

	public string GetJsonString (string query = "")
	{
		if (query.Length == 0) {
			return rawData;
		}

		string[] keys = query.Split ('.');

		if (keys.Length > 1) {
			string first = keys [0];

			if (!readers.ContainsKey (first))
				throw new System.ArgumentException ("JSON does not have key '" + first + "' for query '" + query + "'");
			JSONReader reader = readers [first];
			string newQuery = query.Substring (first.Length + 1);
			return reader.GetJsonString (newQuery);
		} else {
			if (!readers.ContainsKey (query)) {
				string message = "";
				foreach (var key in readers.Keys)
					message += ", " + key;
				message = message.Substring (2);
				throw new System.ArgumentException ("JSON does not have key '" + query + "' keys = " + message);
			}
			return readers [query].GetJsonString ("");
		}
	}

	private void ParseData (string data)
	{		
		data = data.Trim ();
		this.rawData = data;
		readers = new Dictionary<string, JSONReader> ();

		// for just strings, no key/value parsing required.
		if (data.StartsWith ("\"") && data.EndsWith ("\""))
			return;

		//strip outer curly brackets
		if (data.StartsWith ("{") && data.EndsWith ("}"))
			data = data.Substring (1, data.Length - 2);

		string key = "";
		string value = "";
		ParseMode parseMode = ParseMode.AfterValue;
		int curlyCount = 0;
		int bracketCount = 0;
		int doubleQuoteCount = 0;
		bool previousWasEscapeSlash = false;

		foreach (char c in data.ToCharArray()) {
			if (parseMode == ParseMode.Key) {
				if (c == ':') {
					parseMode = ParseMode.Value;
					continue;
				} else {
					key += c;
				}
			} else if (parseMode == ParseMode.Value) {	

				value += c;
				
				if (!previousWasEscapeSlash) {
					if (doubleQuoteCount % 2 == 0) {
						if (c == '{')
							curlyCount++;
						if (c == '[')
							bracketCount++;
						if (c == '}')
							curlyCount--;
						if (c == ']')
							bracketCount--;
					}
					if (c == '"')
						doubleQuoteCount++;
				}

				bool endOfArray = bracketCount == 0 && doubleQuoteCount % 2 == 0 && curlyCount == 0 && c == ',' && !previousWasEscapeSlash;
				bool endOfObject = curlyCount == 0 && doubleQuoteCount % 2 == 0 && c == '}' && !previousWasEscapeSlash;
				if (endOfArray || endOfObject) {
					if (value.EndsWith (","))
						value = value.Substring (0, value.Length - 1);

					readers.Add (key, new JSONReader (value));
					key = "";
					value = "";
					parseMode = ParseMode.AfterValue;
				}

			} else if (parseMode == ParseMode.AfterValue) {
				if (char.IsLetter (c)) {
					parseMode = ParseMode.Key;
					key = c.ToString ();
				}
			}

			previousWasEscapeSlash = c.Equals ('\\') && !previousWasEscapeSlash;
		}
		if (key.Length > 0) {
			readers.Add (key, new JSONReader (value));
		}
	}

	private enum ParseMode
	{
		Key,
		Value,
		AfterValue
	}
}

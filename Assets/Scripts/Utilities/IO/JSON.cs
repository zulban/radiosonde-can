using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.Reflection;
using System.Linq;
using System;
using UnityEngine;

public static class JSON
{
	/// <summary>
	/// throws an exception if the provided data is not a valid JSON.
	/// </summary>
	public static void AssertValid (string data)
	{
		if (!IsValid (data))
			throw new System.ArgumentException ("Not a valid JSON. " + GetValidationErrorMessage (data) + "\n\n<JSON>\n" + data + "\n</JSON>\n");
	}

	public static void SetObjectWithJson (object obj, string data)
	{
		JSONReader reader = new JSONReader (data);

		FieldInfo[] fieldInfos = obj.GetType ().GetFields (BindingFlags.Instance | BindingFlags.Public);

		foreach (var fieldName in reader.GetKeys()) {
			bool usedThisKey = false;
			foreach (FieldInfo fieldInfo in fieldInfos) {		
				if (fieldInfo.Name.Equals (fieldName) && !fieldInfo.IsNotSerialized) {
					usedThisKey = true;

					System.Type t = fieldInfo.FieldType;
					if (t == typeof(int)) {
						fieldInfo.SetValue (obj, reader.GetInt (fieldName));
					} else if (t == typeof(float)) {
						fieldInfo.SetValue (obj, reader.GetFloat (fieldName));
					} else if (t.IsEnum) {
						fieldInfo.SetValue (obj, Enum.Parse (t, reader.GetString (fieldName)));
					} else if (t == typeof(bool)) {
						fieldInfo.SetValue (obj, reader.GetBool (fieldName));
					} else if (t == typeof(DateTime)) {
						fieldInfo.SetValue (obj, reader.GetDateTime (fieldName));
					} else if (t == typeof(string)) {
						fieldInfo.SetValue (obj, reader.GetString (fieldName));
					} else if (t == typeof(bool[])) {
						bool[] array = ClobberArrays<bool> ((bool[])fieldInfo.GetValue (obj), reader.GetBoolArray (fieldName));
						fieldInfo.SetValue (obj, array);
					} else if (t == typeof(int[])) {
						int[] array = ClobberArrays<int> ((int[])fieldInfo.GetValue (obj), reader.GetIntArray (fieldName));
						fieldInfo.SetValue (obj, array);
					} else if (t == typeof(bool[,])) {

						// verify if unimplemented 2D clobber is necessary
						bool[,] array1 = (bool[,])fieldInfo.GetValue (obj);
						bool[,] array2 = reader.Get2DBoolArray (fieldName);
						if (array1.GetLength (0) != array2.GetLength (0) || array1.GetLength (1) != array2.GetLength (1))
							throw new System.ArgumentException ("ClobberArray not implemented for 2D arrays. Arrays must be the same size for SetObjectWithJson");
						
						fieldInfo.SetValue (obj, array2);
					} else {
						object child = fieldInfo.GetValue (obj);
						string value = reader.GetJsonString (fieldName);
						SetObjectWithJson (child, value);
					}
					break;
				}
			}
			if (!usedThisKey)
				throw new System.ArgumentException ("JSON contained keys not found in the object '" + obj + "'. \n<JSON>\n" + data + "\n</JSON>");
		}
	}

	/// <summary>
	/// Returns a copy of array2. However, if array2 is shorter than array1, pads the end values with the array 1 values.
	/// Example:
	///     ClobberArrays([1,2,3], [5,5]) = [5,5,3]
	/// </summary>
	public static T[] ClobberArrays<T> (T[] array1, T[] array2)
	{
		if (array2.Length >= array1.Length)
			return array2;
		
		T[] result = new T[array1.Length];
		for (int i = 0; i < result.Length; i++)
			result [i] = i >= array2.Length ? array1 [i] : array2 [i];
		return result;
	}

	public static bool IsValid (string data)
	{
		return GetValidationErrorMessage (data).Length == 0;
	}

	/// <summary>
	/// This is a hacky partial implementation of serializing an object to JSON.
	/// Unfortunately this seems necessary as there is no simple JSON library 
	/// to serialize the datatypes needed in both a Unity and external C# project.
	/// 
	/// For a generic object, the fields explored correspond to the type of obj. However
	/// if objTypeOverride is used (which could be the type of obj's parent) only those fields are used instead.
	/// 
	/// The if-else blocks show which values are serializable.
	/// </summary>
	public static string ToString (object obj, Type objTypeOverride = null)
	{
		if (obj == null)
			return "\"\"";
		
		System.Type t = obj.GetType ();

		if (t == typeof(bool)) {
			return (bool)obj ? "true" : "false";
		} else if (t == typeof(int) || t == typeof(float) || t.IsEnum) {
			return  obj.ToString ();
		} else if (t == typeof(string)) {
			return '"' + GetEscapedString ((string)obj) + '"';
		} else if (t == typeof(DateTime)) {
			return ((DateTime)obj).ToUnixTimeSeconds ().ToString ();
		} else if (t == typeof(char)) {
			return '"' + (string)obj + '"';
		} else if (t == typeof(int[])) {
			string result = "[";
			int[] array = ((int[])obj);
			for (int i = 0; i < array.Length; i++) {
				if (i > 0)
					result += ",";
				result += array [i].ToString ();
			}	
			return result + "]";
		} else if (t == typeof(bool[])) {
			string result = "";
			bool[] array = ((bool[])obj);
			if (array.GetLength (0) == 0)
				return "[]";
			for (int i = 0; i < array.GetLength (0); i++)
				result += "," + (array [i] ? "1" : "0");
			return "[" + result.Substring (1) + "]";
		} else if (t == typeof(bool[,])) {
			string result = "";
			bool[,] array = ((bool[,])obj);
			for (int j = 0; j < array.GetLength (1); j++) {						
				result += "\n[";
				for (int i = 0; i < array.GetLength (0); i++) {
					if (i > 0)
						result += ",";
					result += array [i, j] ? "1" : "0";
				}
				result += "]";
				if (j < array.GetLength (1) - 1)
					result += ",";
			}
			return "[" + AddIndentToString (result) + "\n]";
		} else {
			// Recursion case for objects
			StringBuilder sb = new StringBuilder ();

			if (objTypeOverride == null)
				objTypeOverride = obj.GetType ();
			FieldInfo[] fieldInfos = objTypeOverride.GetFields (BindingFlags.Instance | BindingFlags.Public);

			foreach (FieldInfo fieldInfo in fieldInfos) {
				if (fieldInfo.IsNotSerialized)
					continue;
				string prefix = "\n" + fieldInfo.Name + ": ";
				sb.Append (prefix + ToString (fieldInfo.GetValue (obj)) + ",");
			}

			// Remove first newline and last comma
			string content = sb.ToString ();
			if (content.Length > 1)
				content = content.Substring (1, content.Length - 2);
			return "{\n" + AddIndentToString (content) + "\n}";
		}
	}

	private static string GetEscapedString (string data)
	{
		if (!data.Contains ('"'))
			return data;

		StringBuilder sb = new StringBuilder ();
		foreach (char c in data.ToCharArray()) {
			if (c == '"' || c == '\\')
				sb.Append ('\\');
			sb.Append (c);
		}
		return sb.ToString ();
	}

	private static string GetValidationErrorMessage (string data)
	{
		data = data.Trim ();

		int curlyCounter = 0;
		int bracketCounter = 0;
		int quoteCounter = 0;
		bool previousWasEscapeSlash = false;
		foreach (char c in data.ToCharArray()) {
			if (!previousWasEscapeSlash) {

				if (quoteCounter % 2 == 0) {
					if (c == '{')
						curlyCounter++;
					if (c == '[')
						bracketCounter++;

					if (c == '}')
						curlyCounter--;
					if (c == ']')
						bracketCounter--;
				}

				if (c == '"')
					quoteCounter++;			
			}

			previousWasEscapeSlash = c.Equals ('\\');
		}

		if (curlyCounter != 0)
			return "Bad number of curly brackets. curlyCounter = " + curlyCounter;

		if (bracketCounter != 0)
			return "Bad number of square brackets. bracketCounter = " + bracketCounter;

		if (quoteCounter % 2 != 0)
			return "Bad number of double quotes. quoteCounter = " + quoteCounter;

		//valid JSON
		return "";
	}

	/// <summary>
	/// Adds indentation to a block of text containing newlines.
	/// Does not add indentation if an unescaped quote char is still open.
	/// </summary>
	private static string AddIndentToString (string text)
	{
		string indent = "    ";
		StringBuilder sb = new StringBuilder (indent);
		int quoteCount = 0;
		bool previousWasEscape = false;
		foreach (char c in text.ToCharArray()) {
			if (!previousWasEscape && c == '"')
				quoteCount++;

			sb.Append (c);
			if (quoteCount % 2 == 0 && c == '\n') {
				sb.Append (indent);
			}				

			previousWasEscape = c == '\\';				
		}

		return sb.ToString ();
	}
}

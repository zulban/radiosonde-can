﻿
using System.Collections;
using System.Linq;
using System.IO;
using UnityEngine;

public static class CSVReader
{
	public static float[,] ParseFloatCSV (string text)
	{
		string[,] stringData = SplitCsvGrid (text);
		float[,] floatData = new float[stringData.GetLength (0), stringData.GetLength (1)];
		//Debug.Log ("(0,0) = " + stringData [0, 0]);
		//Debug.Log ("(2,1) = " + stringData [2, 1]);

		for (int i = 0; i < floatData.GetLength (0); i++) {
			if (stringData [i, 0] == null)
				continue;
			for (int j = 0; j < floatData.GetLength (1); j++) {
				floatData [i, j] = float.Parse (stringData [i, j]);
			}
		}
		return floatData;
	}

	public static string[,] SplitCsvGrid (string csvText, char delimiter=',')
	{
		string[] lines = csvText.Split ('\n');

		int width = lines [0].Split (delimiter).Length;

		string[,] data = new string[width, lines.Length - 1];

		for (int y = 1; y < lines.Length; y++) {
			string[] row = lines [y].Split (delimiter);
			
			if(row.Length!=width)
				continue;

			for (int x = 0; x < width; x++) {
				data [x, y - 1] = row [x];
			}
		}

		return data;
	}
}
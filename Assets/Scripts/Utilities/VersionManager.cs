﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class VersionManager : Singleton<VersionManager>
{
	[SerializeField]
	protected Text versionText;

	public void RefreshVersionText ()
	{
		if (versionText == null) {
			Debug.Log (this + " RefreshVersionText failed. Null values.");
			return;
		}

		versionText.text = PrettyVersionString;
	}

	private string PrettyVersionString{ get { return "version " + Application.version; } }

	private string DetailedVersionString {
		get {
			string message = "unknown version";

			#if UNITY_EDITOR
			System.DateTime dt = System.DateTime.Now;		
			message = "version built on: " + dt.ToString ("yyyy-MM-dd HH:mm:ss");
			message += "\nunityVersion = " + Application.unityVersion;
			message += "\nidentifier = " + Application.identifier;
			message += "\nsystemLanguage = " + Application.systemLanguage;
			message += "\nbundleVersion = " + PlayerSettings.bundleVersion;
			message += "\nAndroid.bundleVersionCode = " + PlayerSettings.Android.bundleVersionCode;
			message += "\ncompanyName = " + PlayerSettings.companyName;
			message += "\nproductName = " + PlayerSettings.productName;
			message += "\napplicationIdentifier = " + PlayerSettings.applicationIdentifier;
			message += "\nscriptingRuntimeVersion = " + PlayerSettings.scriptingRuntimeVersion;
			message += "\nscriptingBackend = " + PlayerSettings.GetScriptingBackend (EditorUserBuildSettings.selectedBuildTargetGroup);
			#endif

			return message;
		}
	}
}

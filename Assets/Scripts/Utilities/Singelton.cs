﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Singleton<T> : MonoBehaviourWithSetup where T : MonoBehaviourWithSetup
{
	private static T instance;

	/// <summary>
	/// Returns the instance of this singleton.
	/// </summary>
	public static T Instance {
		get {
			if (instance == null) {
				instance = Resources.FindObjectsOfTypeAll<T> ().First ();
				if (instance == null) {
					Debug.LogError ("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
				}
			}

			return instance;
		}
	}

	protected override void Setup ()
	{
	}
}
using System;

/// <summary>
/// This is a serializable substitute for Unity's Vector2Int.
/// If changes need to be made to it, it should behave precisely like Vector2Int.
/// </summary>
[System.Serializable]
public struct Point2Int
{
	public int x, y;

	public Point2Int (int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int this [int key] {
		get {
			if (key == 0)
				return x;
			else if (key == 1)
				return y;
			else
				throw new System.IndexOutOfRangeException ("Point2Int index must be 0 or 1. index = " + key);
		}
		set {
			if (key == 0)
				x = value;
			else if (key == 1)
				y = value;
			else
				throw new System.IndexOutOfRangeException ("Point2Int index must be 0 or 1. index = " + key);
		}
	}

	public override bool Equals (object obj)
	{
		if (obj is Point2Int)
			return Equals ((Point2Int)obj);
		else
			return base.Equals (obj);
	}

	public bool Equals (Point2Int vec)
	{
		if (this.x == vec.x && this.y == vec.y)
			return true;
		else
			return false;
	}

	public static bool operator == (Point2Int v1, Point2Int v2)
	{
		if (Object.ReferenceEquals (v1, v2)) {
			return true;
		}
		return (v1.x == v2.x) && (v1.y == v2.y);
	}

	public static bool operator != (Point2Int v1, Point2Int v2)
	{
		if (Object.ReferenceEquals (v1, v2)) {
			return false;
		}
		return !((v1.x == v2.x) && (v1.y == v2.y));
	}

	public static Point2Int operator + (Point2Int v1, Point2Int v2)
	{
		return new Point2Int (v1.x + v2.x, v1.y + v2.y);
	}

	public static Point2Int operator - (Point2Int v1, Point2Int v2)
	{
		return new Point2Int (v1.x - v2.x, v1.y - v2.y);
	}

	public override int GetHashCode ()
	{
		int hash = 17;
		hash = hash * 23 + x.GetHashCode ();
		hash = hash * 23 + y.GetHashCode ();
		return hash;
	}

	public override string ToString ()
	{
		return "<Point2Int (" + x + "," + y + ")>";
	}

	public static implicit operator string (Point2Int point)
	{
		return point.ToString ();
	}
}

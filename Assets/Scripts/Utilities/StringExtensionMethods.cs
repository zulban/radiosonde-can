﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// Extension methods for strings:
///  - without any Unity dependencies.
///  - useful for any C# project.
/// </summary>
public static class StringExtensionMethods
{
	/// <summary>
	/// Capitalize the first letter of this string.
	/// </summary>
	public static string Capitalize (this string text)
	{
		return text.ToUpper ().Substring (0, 1) + text.Substring (1);
	}

	/// <summary>
	/// Removes spaces, tabs, and newslines from the start and end of a string. Mysteriously absent from Unity C#.
	/// </summary>
	public static string Trim (this string text)
	{
		int startIndex = 0;
		int endIndex = text.Length - 1;
		var charArray = text.ToCharArray ();
		while (startIndex < charArray.Length && charArray [startIndex] != ' ' && charArray [startIndex] != '\n' && charArray [startIndex] != '\t')
			startIndex++;
		while (endIndex > 0 && charArray [endIndex] != ' ' && charArray [endIndex] != '\n' && charArray [endIndex] != '\t')
			endIndex--;
		return text.Substring (startIndex, endIndex - startIndex);
	}

	/// <summary>
	/// Why C# doesn't have this overload built into the language I do not know.
	/// </summary>
	public static string[] Split (this string toSplit, string delimit)
	{
		if (toSplit.Length == 0)
			return new string[0];
		return toSplit.Split (new string[]{ delimit }, System.StringSplitOptions.None);
	}

	public static string ToStringWithValues (this int[] values)
	{
		string result = "";
		foreach (var value in values)
			result += ", " + value;
		return "[ " + result.Substring (1) + " ]";
	}

	public static string ToStringWithValues (this List<float> values)
	{
		string result = "";
		foreach (var value in values)
			result += ", " + value;
		return "[ " + result.Substring (1) + " ]";
	}

	public static string ToStringWithValues (this bool[] array)
	{
		string result = "";
		foreach (bool value in array)
			result += ", " + value;
		return "[ " + result.Substring (1) + " ]";
	}

	public static string ToStringWithValues (this int[,] array)
	{
		return ToStringWithValues (array.To2DFloatArray (), 0);
	}

	public static string ToStringWithValues (this float[,] array, int decimals = 2)
	{
		string formatter = "0." + new string ('0', decimals);

		int digits = 1;
		foreach (float a in array)
			digits = Math.Max (digits, a.ToString (formatter).Length);		

		string result = "\n[\n";
		for (int i = 0; i < array.GetLength (0); i++) {
			result += "      [";
			for (int j = 0; j < array.GetLength (1); j++) {

				string number = array [i, j] != 0 ? array [i, j].ToString (formatter) : array [i, j].ToString ();				
				result += number.PadLeft (digits);
				
				if (j < array.GetLength (1) - 1)
					result += ", ";
			}
			result += "]\n";
		}
		return result + "]\n      (top left is (0,0)";
	}

	public static string ToStringWithValues (this bool[,] array)
	{
		string result = "\n[\n";
		for (int i = 0; i < array.GetLength (0); i++) {
			result += "      [";
			for (int j = 0; j < array.GetLength (1); j++) {
				result += array [i, j] ? "1" : "0";
				if (j < array.GetLength (1) - 1)
					result += ", ";
			}
			result += "]\n";
		}
		return result + "]\n      (top left is (0,0)";
	}

	/// <summary>
	/// This prints a string list and its values to look like Python lists [1,2,3].
	/// If there are lots of elements, separates by newlines.
	/// </summary>
	private static string StringListToPrettyStringArray (List<string> items, char startChar = '[', char endChar = ']', bool addDoubleQuotes = true, bool alwaysNewline = false)
	{
		if (items.Count == 0)
			return startChar + "" + endChar;

		string doubleQuotes = addDoubleQuotes ? "\"" : "";
		string newline = (alwaysNewline || items.Count > 10) ? "\n    " : "";
		string commaNewLine = doubleQuotes + ", " + newline + doubleQuotes;
		string result = string.Join (commaNewLine, items.ToArray ());
		return newline + startChar + newline + doubleQuotes + result + doubleQuotes + newline + endChar;
	}

	public static string ToStringWithValues<T,S> (this Dictionary<T,S> dictionary)
	{
		List<String> lines = (from key in dictionary.Keys
		                      select "\"" + key + "\" : \"" + dictionary [key] + "\"").ToList ();
		return StringListToPrettyStringArray (lines, '{', '}', false);
	}

	public static string ToStringWithValues (this IEnumerable<char> chars)
	{
		return StringListToPrettyStringArray ((from c in chars
		                                       select c.ToString ()).ToList ());
	}

	public static string ToStringWithValues (this IEnumerable<String> strings)
	{
		return StringListToPrettyStringArray (strings.ToList ());
	}

	/// <summary>
	/// Return a grammatically correct English list. Examples:
	/// 
	/// [cat] = "cat"
	/// [cat,dog] = "cat or dog"
	/// [cat,dog,turtle] = "cat, dog, or turtle"
	/// </summary>
	public static string ToCommaGrammar (this string[] strings, string orText = "or")
	{
		if (strings.Length < 2)
			return strings [0];
		else if (strings.Length == 2)
			return string.Join (" " + orText + " ", strings);

		//many
		string result = string.Join (", ", strings);
		int i = result.LastIndexOf (',');
		return result.Substring (0, i + 1) + " " + orText + " " + result.Substring (i + 2);
	}

	public static string ToCommaGrammar (this List<string> strings, string orText = "or")
	{
		return strings.ToArray ().ToCommaGrammar (orText);
	}
}

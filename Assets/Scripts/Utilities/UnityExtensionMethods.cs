﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Extension methods for classes with Unity dependencies.
/// </summary>
public static class UnityExtensionMethods
{
	public static T GetOrAddComponent<T> (this GameObject gameObject) where T : UnityEngine.Component
	{
		T component = gameObject.GetComponent<T> ();
		if (component == null)
			component = gameObject.AddComponent<T> ();
		return component;
	}

	public static void ScrollToMinimum (this ScrollRect scrollRect)
	{
		scrollRect.verticalNormalizedPosition = 1;
		scrollRect.horizontalNormalizedPosition = 0;
	}

	/// <summary>
	/// This fills the RectTransform with clones of the rowPrefab, and resizes the RectTransform to fit them.
	/// </summary>
	public static List<GameObject> PackWithRows (this RectTransform containerRT, GameObject prefab, int itemCount, bool isVertical = true)
	{		
		// Verify that anchors have required settings
		RectTransform prefabRT = prefab.GetComponent<RectTransform> ();
		if (prefabRT.anchorMin.x != 0.5f || prefabRT.anchorMax.x != 0.5f || prefabRT.anchorMin.y != 0.5f || prefabRT.anchorMax.y != 0.5f) {
			Debug.LogError ("PackWithRows failed. Prefab anchor must be (0.5,0.5) and (0.5,0.5)");
			return new List<GameObject> ();
		}

		//calculate the width and height of each child item.
		float width, height;
		if (isVertical) {
			width = containerRT.rect.width;
			float ratio = width / prefabRT.rect.width;
			height = prefabRT.rect.height * ratio;
		} else {
			height = containerRT.rect.height;
			float ratio = height / prefabRT.rect.height;
			width = prefabRT.rect.width * ratio;
		}

		//adjust the height or width of the container so that it will just barely fit all its children
		//vertical packs have adjusted width, horizontal packs have adjusted height
		if (isVertical) {
			float scrollHeight = height * itemCount;
			containerRT.offsetMin = new Vector2 (containerRT.offsetMin.x, -scrollHeight / 2);
			containerRT.offsetMax = new Vector2 (containerRT.offsetMax.x, scrollHeight / 2);
		} else {
			float scrollWidth = width * itemCount;
			containerRT.offsetMin = new Vector2 (-scrollWidth / 2, containerRT.offsetMin.y);
			containerRT.offsetMax = new Vector2 (scrollWidth / 2, containerRT.offsetMax.y);
		}

		List<GameObject> items = new List<GameObject> ();

		//create a new item, name it, and set the parent
		for (int i = 0; i < itemCount; i++) {
			GameObject newItem = GameObject.Instantiate (prefab) as GameObject;
			items.Add (newItem);
			newItem.name = prefab.name + " (" + i + ")";
			newItem.transform.SetParent (containerRT.gameObject.transform);

			//move and size the new item
			RectTransform itemRT = newItem.GetComponent<RectTransform> ();

			float x, y;
			if (isVertical) {
				x = -containerRT.rect.width / 2;
				y = containerRT.rect.height / 2 - height * (i + 1);
			} else {
				x = containerRT.rect.width / 2 - width * Mathf.Abs (itemCount - i);
				y = -containerRT.rect.height / 2;
			}
			itemRT.offsetMin = new Vector2 (x, y);

			x = itemRT.offsetMin.x + width;
			y = itemRT.offsetMin.y + height;
			itemRT.offsetMax = new Vector2 (x, y);

			//dynamically created pieces have their local scale changed, set it back
			itemRT.localScale = Vector3.one;
		}

		return items;
	}
}

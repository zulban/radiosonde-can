﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefSerialization : MonoBehaviour
{
	public static T LoadObject<T> (string playerPrefKey, bool verbose = false)
	{
		string data = PlayerPrefs.GetString (playerPrefKey);		
		if (verbose) {
			Debug.Log ("LoadObject JSON string = \n" + data);
		}
		T obj = System.Activator.CreateInstance<T> ();
		try {
			JSON.SetObjectWithJson (obj, data);
		} catch (System.Exception exception) {
			string message = "Could not set values in object with JSON. Returning default object.\n<JSON>\n" + data + "\n</JSON>\nexception = " + exception;
			Debug.Log (message);
		}
		return obj;
	}

	public static void SaveObject<T> (T obj, string playerPrefKey, bool verbose = false)
	{
		string data = JSON.ToString (obj);
		if (verbose) {
			Debug.Log ("SaveObject JSON.ToString (obj) = \n" + data);
		}
		PlayerPrefs.SetString (playerPrefKey, data);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// Extension methods:
///  - without any Unity dependencies.
///  - useful for any C# project.
/// </summary>
public static class ExtensionMethods
{
	/// <summary>
	/// Returns Unix time stamp in seconds.
	/// C# 3.5 apparently does not have this!
	/// </summary>
	public static int ToUnixTimeSeconds (this DateTime dateTime)
	{
		return (int)(dateTime.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;
	}

	/// <summary>
	/// Returns a DateTime object corresponding to this unix seconds value.
	/// </summary>
	public static DateTime ConvertUnixTimeStampSeconds (this int unixSeconds)
	{
		return new DateTime (1970, 1, 1, 0, 0, 0).AddSeconds (unixSeconds);
	}

	/// <summary>
	/// Proportionally adjusts all values in the array to values between [minValue,maxValue]
	/// </summary>
	public static void Normalize (this float[,] array, float minimum = 0, float maximum = 1)
	{
		float arrayMax = array.Max ();
		float arrayMin = array.Min ();
		float arrayDelta = arrayMax - arrayMin;
		float delta = maximum - minimum;

		// if all elements are equal, do nothing instead of division by zero error
		if (arrayDelta == 0)
			return;

		for (int i = 0; i < array.GetLength (0); i++) {
			for (int j = 0; j < array.GetLength (1); j++) {
				array [i, j] = minimum + ((array [i, j] - arrayMin) / arrayDelta) * delta;
			}
		}
	}

	public static float[,] To2DFloatArray (this int[,] array)
	{
		float[,] floatArray = new float[array.GetLength (0), array.GetLength (1)];
		for (int i = 0; i < array.GetLength (0); i++) {
			for (int j = 0; j < array.GetLength (1); j++) {
				floatArray [i, j] = array [i, j];
			}
		}
		return floatArray;
	}

	public static float Min (this float[,] array)
	{
		float value = float.PositiveInfinity;
		foreach (float a in array)
			value = Mathf.Min (a, value);
		return value;
	}

	public static float Max (this float[,] array)
	{
		float value = float.NegativeInfinity;
		foreach (float a in array)
			value = Mathf.Max (a, value);
		return value;
	}

	/// <summary>
	/// Returns a new, smaller array. Fills with values between start and end, both inclusive.
	/// This is an alternative to the verbosity and multi-step processes in native C#.
	/// </summary>
	public static int[] Subset (this int[] array, int startIndex, int endIndex)
	{
		int count = endIndex - startIndex + 1;
		int[] newArray = new int[count];
		for (int i = 0; i < count; i++)
			newArray [i] = array [startIndex + i];
		return newArray;
	}

	/// <summary>
	/// Randomly shuffle the list. If randomSeed = 0, use some random seed instead.
	/// </summary>
	public static void Shuffle<T> (this IList<T> list, int randomSeed = 0)
	{  
		System.Random rng;
		if (randomSeed == 0)
			rng = new System.Random ();
		else
			rng = new System.Random (randomSeed);
		
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next (n + 1);  
			T value = list [k];  
			list [k] = list [n];  
			list [n] = value;  
		}  
	}

	public static void AddIfNotContains<T>(this IList<T> list, T item){
		if (!list.Contains (item))
			list.Add (item);
	}

	/// <summary>
	/// Returns true if any two items in this collection match, uses Equals method.
	/// </summary>
	public static bool HasDuplicates<T> (this IEnumerable<T> items)
	{
		return items.Count () != items.Distinct ().Count ();
	}

	/// <summary>
	/// Returns a new dictionary, except swapping its keys with its values.
	/// </summary>
	public static Dictionary<TValue, TKey> Reversed<TKey, TValue> (this IDictionary<TKey, TValue> source)
	{
		var dictionary = new Dictionary<TValue, TKey> ();
		foreach (var entry in source) {
			if (!dictionary.ContainsKey (entry.Value))
				dictionary.Add (entry.Value, entry.Key);
		}
		return dictionary;
	}

	public static System.Type[] GetAllDerivedTypes (this System.AppDomain aAppDomain, System.Type aType)
	{
		var result = new List<System.Type> ();
		var assemblies = aAppDomain.GetAssemblies ();
		foreach (var assembly in assemblies) {
			var types = assembly.GetTypes ();
			foreach (var type in types) {
				if (type.IsSubclassOf (aType))
					result.Add (type);
			}
		}
		return result.ToArray ();
	}

	/// <summary>
	/// Deep clone the object. Note: this is a slow but thorough operation.
	/// </summary>
	public static T DeepClone<T> (this T a)
	{
		using (var ms = new MemoryStream ()) {
			var formatter = new BinaryFormatter ();
			formatter.Serialize (ms, a);
			ms.Position = 0;
			return (T)formatter.Deserialize (ms);
		}
	}

	/// <summary>
	/// Returns the mathematical variance for this list of numbers, for [startIndex,endIndex], inclusive.
	/// </summary>
	public static float Variance (this List<float> numbers)
	{
		return Variance (numbers, 0, numbers.Count - 1);
	}

	/// <summary>
	/// Returns the mathematical variance for this list of numbers, for [startIndex,endIndex], inclusive.
	/// </summary>
	public static float Variance (this List<float> numbers, int startIndex, int endIndex)
	{
		if (numbers.Count == 0)
			return 0;
		if (startIndex < 0 || endIndex >= numbers.Count || startIndex >= endIndex)
			throw new System.ArgumentException ("Cannot calculate variance on these numbers with these indexes. [" + startIndex + "," + endIndex + "]");
		
		float variance = 0;
		for (int i = startIndex; i <= endIndex; i++) {
			variance += Mathf.Pow (numbers.Mean () - numbers [i], 2);
		}
		return variance / (endIndex - startIndex + 1);
	}

	public static float StandardDeviation (this List<float> numbers)
	{
		return numbers.StandardDeviation (0, numbers.Count - 1);
	}

	public static float StandardDeviation (this List<float> numbers, int startIndex, int endIndex)
	{
		return Mathf.Sqrt (Variance (numbers, startIndex, endIndex));
	}

	/// <summary>
	/// Returns the mathematical mean.
	/// </summary>
	public static float Mean (this List<float> numbers)
	{
		return numbers.Mean (0, numbers.Count - 1);
	}

	/// <summary>
	/// Returns the mathematical mean, for [startIndex,endIndex], inclusive.
	/// </summary>
	public static float Mean (this List<float> numbers, int startIndex, int endIndex)
	{
		if (startIndex < 0 || endIndex >= numbers.Count || startIndex >= endIndex)
			throw new System.ArgumentException ("Cannot calculate mean on these numbers with these indexes. startIndex = "
			+ startIndex + " endIndex = " + endIndex + " numbers.Count = " + numbers.Count);
		
		float mean = 0;
		for (int i = startIndex; i <= endIndex; i++)
			mean += numbers [i];
		return mean / (endIndex - startIndex + 1);
	}

	/// <summary>
	/// Mutates the array, adding each item from array2 to it. Array sizes must match.
	/// </summary>
	public static void Sum (this int[] array1, int[] array2)
	{
		if (array1.Count () != array2.Count ())
			throw new System.ArgumentException ("Cannot sum arrays of different length. " + array1.Count () + " != " + array2.Count ());

		for (int i = 0; i < array1.Count (); i++)
			array1 [i] += array2 [i];
	}
}

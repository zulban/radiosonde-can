﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractScrollable : RefreshableUI, ISetupInspector
{
	[SerializeField]
	protected bool cycle = true;

	[SerializeField]
	protected Button decreaseButton, increaseButton;

	[SerializeField]
	[Tooltip ("If increase or decrease button are null, attempt to find automatically.")]
	protected bool automaticallyFindButtons = true;

	[SerializeField]
	protected bool automaticallyFindImage = true;

	[SerializeField]
	[Tooltip ("Updates the button image on value changes. Appears disabled if scroll index is 0.")]
	private bool changeButtonImage = false;

	protected int index;
	protected Point2Int indexRange;
	private Image buttonImage;

	public override void RefreshAppearance ()
	{
		SetupIfSetupHasNotRun ();

		if (changeButtonImage && buttonImage != null) {
			buttonImage.sprite = ResourceManager.Instance.GetButtonSprite (index != 0);
		}
	}

	/// <summary>
	/// Attempts to find the decrease button, increase button, and image if one is attached
	/// </summary>
	public virtual void FindValuesForInspector ()
	{
		if (automaticallyFindButtons) {
			foreach (GameObject child in gameObject.GetAllChildren()) {			
				Button button = child.GetComponent<Button> ();
				if (decreaseButton == null) {
					decreaseButton = button;
				} else if (increaseButton == null) {
					increaseButton = button;
				}
			}
		}

		if (automaticallyFindImage) {
			buttonImage = GetComponent<Image> ();
			if (changeButtonImage && buttonImage == null) {
				Debug.LogError (this + " cannot change button appearance, no image found.");
				return;
			}
		}
	}

	public int GetIndex ()
	{
		return index;
	}

	/// <summary>
	/// If newIndex is -1, set to last value.
	/// invokeOnValueChange is useful when many SetIndex are done at once and only one call is needed.
	/// </summary>
	public void SetIndex (int newIndex, bool notifyValueChange = true)
	{
		if (newIndex == -1)
			newIndex = indexRange.y;

		index = newIndex;

		if (notifyValueChange)
			NotifyValueChange ();
		
		RefreshAppearance ();
	}

	public void ClickDecrease ()
	{
		int newIndex = index;

		if (cycle || index > indexRange.x)
			newIndex--;

		if (index < indexRange.x)
			newIndex = indexRange.y;

		SetIndex (newIndex);
	}

	public void ClickIncrease ()
	{
		int newIndex = (index + 1) % (indexRange.y + 1);

		SetIndex (newIndex);
	}

	protected override void Setup ()
	{
		FindValuesForInspector ();
		AddListeners ();
	}

	private void AddListeners ()
	{		
		if (decreaseButton != null)
			decreaseButton.onClick.AddListener (ClickDecrease);

		if (increaseButton != null)
			increaseButton.onClick.AddListener (ClickIncrease);
	}
}

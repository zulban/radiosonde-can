﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BooleanInputField : RefreshableUI
{
	protected bool fieldValue;

	public bool GetValue ()
	{
		return fieldValue;
	}

	public void SetValue (bool newValue, bool refreshAppearance = true, bool notifyValueChange = true)
	{
		fieldValue = newValue;

		if (notifyValueChange)
			NotifyValueChange ();

		if (refreshAppearance)
			RefreshAppearance ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageRadioButtons : RefreshableUI
{
	[SerializeField]
	public int startIndex = 0;

	[SerializeField]
	protected List<Button> radioButtons;

	[SerializeField]
	protected Color activeColorTint = Color.white;

	[SerializeField]
	protected Color inactiveColorTint = Color.white;

	protected bool isSetup = false;

	private int index = -1;
	private bool isEnabled = true;

	public int ButtonCount{ get { return radioButtons.Count; } }

	public void SetButtonsAndRefreshAppearance (List<Button> buttons, int startIndex)
	{
		radioButtons = new List<Button> ();
		radioButtons.AddRange (buttons);
		this.startIndex = startIndex;
		AddButtonListeners ();
		RefreshAppearance ();
	}

	public void SetEnabled (bool isEnabled)
	{
		this.isEnabled = isEnabled;
		for (int i = 0; i < radioButtons.Count; i++) {
			var button = radioButtons [i];
			SetButtonAppearance (button, index == i, isEnabled);
			button.enabled = isEnabled;
		}
	}

	public void FindButtons ()
	{
		radioButtons = new List<Button> ();
		foreach (GameObject go in gameObject.GetAllChildren()) {
			Button button = go.GetComponent<Button> ();
			if (button != null) {
				radioButtons.Add (button);
			}
		}
	}

	public int GetSelectedIndex ()
	{
		return index;
	}

	public void SetIndex (int index)
	{
		if (index < 0 || index > radioButtons.Count)
			index = -1;

		this.index = index;
		RefreshAppearance ();
	}

	public override void RefreshAppearance ()
	{
		for (int i = 0; i < radioButtons.Count; i++) {
			Button button = radioButtons [i];
			if (button != null)
				SetButtonAppearance (button, i == index, isEnabled);
		}
	}

	public void OnClick (Button clickedButton)
	{
		if (clickedButton == null) {
			index = -1;
		} else {
			index = radioButtons.IndexOf (clickedButton);
		}

		RefreshAppearance ();
	}

	protected override void Setup ()
	{		
		if (radioButtons == null || radioButtons.Count == 0) {
			FindButtons ();
		}

		bool colorsAreDefault = activeColorTint == Color.white && inactiveColorTint == Color.white;
		if (colorsAreDefault) {
			activeColorTint = Color.white;
			inactiveColorTint = new Color (0.2f, 0.2f, 0.2f);
		}

		AddButtonListeners ();
		index = startIndex;
		RefreshAppearance ();

		isSetup = true;
	}

	private void AddButtonListeners ()
	{
		foreach (var button in radioButtons) {
			button.onClick.AddListener (delegate {
				OnClick (button);
			});
		}
	}

	private void SetNoButtonsAsSelected ()
	{
		OnClick (null);
	}

	private void SetButtonAppearance (Button button, bool isSelected, bool isEnabled)
	{
		Image image = button.GetComponent<Image> ();

		Color newColor = isSelected ? activeColorTint : inactiveColorTint;
		if (!isSelected && !isEnabled) {
			newColor = inactiveColorTint;
			newColor.r /= 3;
			newColor.g /= 3;
			newColor.b /= 3;
		}
		image.color = newColor;
	}
}

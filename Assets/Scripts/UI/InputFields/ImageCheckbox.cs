﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageCheckbox : BooleanInputField, ISetupInspector
{
	[SerializeField]
	protected Sprite spriteTrue, spriteFalse;

	[SerializeField]
	[Tooltip ("This ensures that clicking anywhere in this RectTransform counts as a checkbox click.")]
	protected bool createImageForClickRegion = true;

	[SerializeField]
	[Tooltip ("Setup will attempt to find checkboxImage automatically. However this requires Start to run before externally invoked appearance updates.")]
	protected Image checkboxImage;

	[SerializeField]
	[Tooltip ("Normally uses a mouse click, requiring down then up. This sets the button to respond to just mouse down.")]
	protected bool isMouseDown = false;

	/// <summary>
	/// Attempt to automatically find the checkboxImage
	/// </summary>
	public void FindValuesForInspector ()
	{		
		if (checkboxImage == null) {
			foreach (var image in ParentChildFunctions.GetComponentsInChildren<Image>(gameObject,false)) {
				checkboxImage = image;
				break;
			}
		}
	}

	public override void RefreshAppearance ()
	{
		checkboxImage.sprite = fieldValue ? spriteTrue : spriteFalse;
	}

	public void ClickButton ()
	{
		SetValue (!fieldValue);
	}

	protected override void Setup ()
	{
		EventButton button = gameObject.GetOrAddComponent<EventButton> ();
		button.AddListener (delegate {
			ClickButton ();
		}, isMouseDown);

		if (createImageForClickRegion) {
			Image image = GetComponent<Image> ();
			if (image == null) {
				image = gameObject.AddComponent<Image> ();
				image.color = Color.clear;
			}
		}

		FindValuesForInspector ();

		if (checkboxImage == null || spriteTrue == null || spriteFalse == null) {
			Debug.LogError (this + " has null values.");
			return;
		}

		RefreshAppearance ();
	}
}

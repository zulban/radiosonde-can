﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiosondeUI : MonoBehaviourWithSetup
{
	private Vector3 startPosition;

	protected override void Setup ()
	{
		startPosition = transform.position;
	}

	void Update ()
	{
		float t = Mathf.Sin (Time.timeSinceLevelLoad * 0.37f);
		transform.position = startPosition + t * new Vector3 (0, 3, 0);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UIUtilities {

	private const float ThinkAnimationSpeed = 200;

	public static void RotateThinkRT (RectTransform rectTransform)
	{
		rectTransform.Rotate (new Vector3 (0, 0, -Time.deltaTime * ThinkAnimationSpeed));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// RereshableUI classes are UI elements that store values in the class which can be used to refresh the appearance whenever.
/// </summary>
public abstract class RefreshableUI : MonoBehaviourWithSetup
{
	public delegate void ValueChangeEvent ();

	public event ValueChangeEvent OnValueChange;

	public abstract void RefreshAppearance ();

	public bool HasListeners{ get { return OnValueChange != null; } }

	void OnEnable ()
	{
		RefreshAppearance ();
	}

	protected void NotifyValueChange ()
	{
		if (OnValueChange != null)
			OnValueChange ();
	}
}

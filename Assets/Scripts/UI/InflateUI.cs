﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InflateUI : Singleton<InflateUI>
{
	[SerializeField]
	protected Image inflateImage;

	private const float minInflateScale = 0.3f;

	public float InflateRatio{ get { return (1 + Mathf.Sin (Time.timeSinceLevelLoad)) / 2; } }

	protected override void Setup ()
	{
		if (inflateImage == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}
	}

	public void SetColor (Color color)
	{
		inflateImage.color = color;
	}

	void Update ()
	{
		float scale = Mathf.Lerp (minInflateScale, 1, InflateRatio);
		inflateImage.transform.localScale = new Vector3 (scale, scale, scale);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radiosonde : MonoBehaviour
{
	private const float rotateSpeed=9;

	void Update ()
	{
		Vector3 angle = rotateSpeed*new Vector3 (0, Time.deltaTime, 0);
		transform.Rotate (angle);
	}
}

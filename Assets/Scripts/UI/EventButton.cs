﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Add this component to a class to have more control over input events than a simple UI.Button.
/// UI.Button requires mouse down and mouse up for a click.
/// </summary>
public class EventButton : MonoBehaviour, IPointerDownHandler, IPointerClickHandler
{
	public delegate void OnMouseDownEvent ();

	private event OnMouseDownEvent OnMouseDown;
	private event OnMouseDownEvent OnMouseClick;

	/// <summary>
	/// Adds a listener for OnPointerClick (mouse down then up). Optionally, just on mouse down.
	/// </summary>
	public void AddListener (OnMouseDownEvent onMouseEvent, bool isMouseDown)
	{
		if (isMouseDown)
			OnMouseDown += onMouseEvent;
		else
			OnMouseClick += onMouseEvent;
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (OnMouseClick != null)
			OnMouseClick.Invoke ();
	}

	public void OnPointerDown (PointerEventData eventData)
	{
		if (OnMouseDown != null)
			OnMouseDown.Invoke ();
	}
}

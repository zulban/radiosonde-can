﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BounceText : Singleton<BounceText>
{
	[SerializeField]
	protected Text text;

	private Vector3 startScale;
	private Quaternion startRotation;

	protected override void Setup ()
	{
		if (text == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		startScale = text.gameObject.transform.localScale;
		startRotation = text.gameObject.transform.rotation;
		
		SetRandomText ();
	}

	public void SetRandomText ()
	{
		Debug.Log ("SetRandomText");
		text.text = LocalisationManager.Instance.GetRandomBounceText ();
	}

	void Update ()
	{
		float t = Mathf.Sin (Time.timeSinceLevelLoad);
		text.transform.localScale = startScale + t * new Vector3 (0.05f, 0, 0.05f);

		t = Mathf.Sin (Time.timeSinceLevelLoad * 1.3f);
		Vector3 newRotation = startRotation.eulerAngles + t * new Vector3 (0, 0, 1.5f);
		text.transform.rotation = Quaternion.Euler (newRotation);
	}
}

﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This simply contains all menu settings like appearance and sounds.
/// Menu settings have no impact on the difficulty of a game for the player or the AI.
/// </summary>
[System.Serializable]
public class MenuSettings
{
	public bool sound = true;
	public bool showLegalMoves = true;
	public bool showLastMove = true;
	public bool glowRuleTiles = true;
}

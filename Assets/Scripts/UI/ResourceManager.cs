﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : Singleton<ResourceManager>
{
	[SerializeField]
	public Sprite activeButton, inactiveButton;

	protected override void Setup ()
	{
		
	}

	public Sprite GetButtonSprite (bool isActive)
	{
		SetupIfSetupHasNotRun ();
		return isActive ? activeButton : inactiveButton;
	}
}

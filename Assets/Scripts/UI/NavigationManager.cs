﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

/// <summary>
/// Navigates between UI pages.
/// </summary>
public class NavigationManager : Singleton<NavigationManager>
{
	[SerializeField]
	protected PageTypes startPage;

	[SerializeField]
	protected PageTransition[] pageTransitions;

	[SerializeField]
	protected Camera playCamera, mainMenuCamera;

	private PageTypes currentPageType;
	private Dictionary<string,PageTypes> pageTypeStrings = new Dictionary<string, PageTypes> ();

	public PageTypes CurrentPageType{ get { return currentPageType; } }

	private float secondsSinceLastActivity;

	public float SecondsSinceLastActivity{ get { return secondsSinceLastActivity; } }

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
			ClickMobileDeviceBack ();

		secondsSinceLastActivity += Time.deltaTime;
	}

	/// <summary>
	/// This is called to track whenever a human interaction or any activity occurs, to know how many seconds since the last one.
	/// </summary>
	public void NotifyActivity ()
	{
		secondsSinceLastActivity = 0;
	}

	public void ClickMenuIcon ()
	{
		LocalisationManager lm = LocalisationManager.Instance;
		string[] labels = new string[] {lm.GetTranslation (Lang.MainMenu),
			lm.GetTranslation (Lang.NewGame),
			lm.GetTranslation (Lang.ExitApp)
		};
		Action[] clickActions = new Action[] {delegate {
				NavigationManager.Instance.SetPage (PageTypes.MainMenu);
			},
			PlayManager.Instance.NewGame,
			ClickExit
		};
		PopupManager.Instance.ShowMenuPopup (labels, clickActions);
	}

	/// <summary>
	/// When the player clicks a "back" or "escape" button native to the mobile device.
	/// </summary>
	public void ClickMobileDeviceBack ()
	{
		NotifyActivity ();

		PopupManager.Instance.ClickPopupBackground ();
	}

	public void ClickBack ()
	{
		NotifyActivity ();

		PopupManager.Instance.ClickPopupBackground ();
		SetPage (PageTypes.MainMenu);
	}

	public void ClickMainMenuPlay ()
	{
		NotifyActivity ();

		SetPage (PageTypes.Play);
	}

	public void SetPage (string pageTypeString)
	{
		NotifyActivity ();

		if (!pageTypeStrings.ContainsKey (pageTypeString)) {
			Debug.LogError ("SetPage failed. No pageTransition has enum with this name: '" + pageTypeString + "'.");
			return;
		}
		SetPage (pageTypeStrings [pageTypeString]);
	}

	/// <summary>
	/// Hide all gameObjects associated with this pageType.
	/// Used in special page transitions, like splash, which 
	/// show gameObjects from two pageTypes at the same time for a bit.
	/// </summary>
	public void HidePage (PageTypes pageType)
	{
		foreach (var pageTransition in pageTransitions) {
			if (pageTransition.pageType != pageType)
				continue;

			foreach (var go in pageTransition.visibleGameObjects)
				go.SetActive (false);
		}
	}

	public void ClickPrivacyPolicy ()
	{
		NotifyActivity ();

		string url = LocalisationManager.Instance.GetTranslation (Lang.LinkPrivacy);	
		Application.OpenURL (url);
	}

	public void ClickTermsOfUse ()
	{
		NotifyActivity ();

		string url = LocalisationManager.Instance.GetTranslation (Lang.LinkTOS);	
		Application.OpenURL (url);	
	}

	public void ClickNWP ()
	{
		NotifyActivity ();

		string url = LocalisationManager.Instance.GetTranslation (Lang.LinkNWP);	
		Application.OpenURL (url);
	}

	public void SetPage (PageTypes pageType, bool hideOtherPages = true)
	{
		NotifyActivity ();

		if (hideOtherPages)
			HideAllPageGameObjects ();
		ShowPageGameObjects (pageType);

		if (pageType == PageTypes.MainMenu)
			BounceText.Instance.SetRandomText ();

		playCamera.gameObject.SetActive (pageType != PageTypes.MainMenu);
		mainMenuCamera.gameObject.SetActive (pageType == PageTypes.MainMenu);

		currentPageType = pageType;
	}

	public void ClickExit ()
	{
		NotifyActivity ();

		PopupManager.Instance.ShowTwoButtonPopup (LocalisationManager.Instance.GetTranslation (Lang.ReallyExit), Constants.Yes, Constants.No,
			delegate {
				Application.Quit ();
			}, null);		
	}

	protected override void Setup ()
	{
		if (pageTransitions.Length == 0 || playCamera == null || mainMenuCamera == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		SetAndVerifyPageStrings ();
		SetPage (startPage);
	}

	/// <summary>
	/// Verify that no PageTransitions have null values (this happens when GameObjects are deleted or replaced, but not updated in PageManager)
	/// also verify no duplicate GameObject in one pageTransition, likely a setup mistake.
	/// </summary>
	private void VerifyPageTransitionNullValues ()
	{
		int index = -1;
		foreach (var pageTransition in pageTransitions) {
			index++;

			HashSet<GameObject> usedGameObjects = new HashSet<GameObject> ();
			foreach (GameObject go in pageTransition.visibleGameObjects) {
				if (go == null) {
					Debug.LogError (this + " has null values in pageTransitions. " + pageTransition.pageType + " index = " + index);
					return;
				}
				if (usedGameObjects.Contains (go)) {
					Debug.LogError (this + " has duplicated GameObject values in pageTransitions. " + pageTransition.pageType + " " + go + " index = " + index);
					return;
				}
				usedGameObjects.Add (go);
			}
		}
	}

	/// <summary>
	/// verify there is exactly one PageTransition for every PageType.
	/// </summary>
	private void SetAndVerifyPageStrings ()
	{
		HashSet<PageTypes> usedPageTypes = new HashSet<PageTypes> ();
		foreach (var pageTransition in pageTransitions) {
			if (usedPageTypes.Contains (pageTransition.pageType)) {
				Debug.LogError (this + " has duplicate pageType in PageTransitions. " + pageTransition.pageType);
				return;
			}
			usedPageTypes.Add (pageTransition.pageType);
		}
		foreach (PageTypes pageType in Enum.GetValues(typeof(PageTypes))) {
			if (!usedPageTypes.Contains (pageType)) {
				Debug.LogError (this + " has no PageTransition for pageType: " + pageType);
				return;
			}
			pageTypeStrings.Add (Enum.GetName (typeof(PageTypes), pageType), pageType);
		}
	}

	private void HideAllPageGameObjects ()
	{
		foreach (var pageTransition in pageTransitions)
			foreach (GameObject go in pageTransition.visibleGameObjects)
				go.SetActive (false);
	}

	private void ShowPageGameObjects (PageTypes pageType)
	{
		foreach (var pageTransition in pageTransitions) {
			bool isVisible = pageTransition.pageType == pageType;
			if (isVisible) {
				foreach (GameObject go in pageTransition.visibleGameObjects) {
					go.SetActive (true);
				}
			}
		}
	}

	private void RecursiveRefreshAppearance (GameObject container)
	{
		foreach (var refreshable in ParentChildFunctions.GetComponentsInChildren<RefreshableUI>(container,false)) {
			refreshable.RefreshAppearance ();
		}
	}

	[System.Serializable]
	public class PageTransition
	{
		public PageTypes pageType;
		public GameObject[] visibleGameObjects;
	}

	[System.Serializable]
	public class BannerGameObjects
	{
		public GameObject large, small, patronSmall, patronLarge;

		public bool HasNulls{ get { return large == null || small == null || patronSmall == null || patronLarge == null; } }
	}
}

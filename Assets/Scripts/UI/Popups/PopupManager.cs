﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : Singleton<PopupManager>
{
	[SerializeField]
	protected GameObject popupMenuGO, popupBackground;

	[SerializeField]
	[Tooltip ("This contains all permanent popups, temporary popups, and the background shadow.")]
	protected GameObject popupsContainer;

	[SerializeField]
	[Tooltip ("This contains all temporary popups and is usually empty.")]
	protected GameObject temporaryPopupsContainer;

	[SerializeField]
	protected GameObject textPopupPrefab;

	public bool IsShowingPopup{ get { return popupsContainer.activeSelf; } }

	private PopupMenu popupMenu;

	private Queue<PopupData> buttonPopupQueue;

	void Update ()
	{
		SetupIfSetupHasNotRun ();

		if (buttonPopupQueue.Count > 0 && !popupsContainer.activeInHierarchy)
			ShowNextPopupInQueue ();
	}

	public void HideAllPopups ()
	{
		popupBackground.SetActive (false);
		popupsContainer.SetActive (false);
		foreach (GameObject popup in popupsContainer.GetAllChildren())
			popup.SetActive (false);
	}

	/// <summary>
	/// This shows the menu when the top right menu icon is clicked.
	/// Depending on the screen, different options will appear.
	/// </summary>
	public void ShowMenuPopup (string[] labels, System.Action[] clickActions)
	{
		if (labels.Length != clickActions.Length)
			throw new System.ArgumentException ("ShowMenuPopup labels and clickActions must have the same length.");

		ShowPopupGameObject (popupMenuGO);
		popupMenu.SetButtons (labels, clickActions);
	}

	/// <summary>
	/// Often this means the user wants to cancel the popup prompt.
	/// </summary>
	public void ClickPopupBackground ()
	{
		HideAllPopups ();
	}

	public void ShowButtonPopup (string message, string[] buttonLabels, System.Action[] buttonActions)
	{
		SetupIfSetupHasNotRun ();
		PopupData popupData = new PopupData (message, buttonLabels, buttonActions);
		EnqueueAndShowIfNecessary (popupData);
	}

	public void ShowNextPopupInQueue ()
	{
		HideAllPopups ();
		if (buttonPopupQueue.Count > 0) {
			PopupData popupData = buttonPopupQueue.Dequeue ();
			ShowPopup (popupData);
		}
	}

	/// <summary>
	/// We cannot just enqueue and pop upon update because there would be a frame flicker in between.
	/// </summary>
	private void EnqueueAndShowIfNecessary (PopupData popupData)
	{
		buttonPopupQueue.Enqueue (popupData);
		if (!IsShowingPopup)
			ShowNextPopupInQueue ();
	}

	private void ShowPopup (PopupData popupData)
	{
		GameObject popup = Instantiate (textPopupPrefab) as GameObject;
		TextPopup textPopup = popup.GetComponent<TextPopup> ();

		if (popupData.buttonLabels.Length < 3)
			textPopup.SetCenterMessage (popupData.message);
		else
			textPopup.SetTopMessage (popupData.message);

		System.Action[] wrappedActions = new System.Action[popupData.buttonActions.Length];
		for (int i = 0; i < popupData.buttonActions.Length; i++) {
			int index = i;
			wrappedActions [i] = delegate {
				Destroy (popup);
				ShowNextPopupInQueue ();
				if (popupData.buttonActions [index] != null)
					popupData.buttonActions [index] ();
			};
		}
			
		textPopup.SetButtons (popupData.buttonLabels, wrappedActions);
		ShowTemporaryPopups ();
		popup.transform.SetParent (temporaryPopupsContainer.transform, false);
	}

	public void ShowOneButtonPopup (string message, string buttonLabel, System.Action buttonAction)
	{
		ShowButtonPopup (message, new string[]{ buttonLabel }, new System.Action[]{ buttonAction });
	}

	public void ShowTwoButtonPopup (string message, string button1Label, string button2Label,
	                                System.Action button1Action, System.Action button2Action)
	{
		ShowButtonPopup (message, new string[]{ button1Label, button2Label }, 
			new System.Action[]{ button1Action, button2Action });
	}

	public void ShowThreeButtonPopup (string message, string button1Label, string button2Label, string button3Label,
	                                  System.Action button1Action, System.Action button2Action, System.Action button3Action)
	{
		ShowButtonPopup (message, new string[]{ button1Label, button2Label, button3Label }, 
			new System.Action[]{ button1Action, button2Action, button3Action });
	}

	public void ShowFourButtonPopup (string message, string button1Label, string button2Label, string button3Label, string button4Label,
	                                 System.Action button1Action, System.Action button2Action, System.Action button3Action, System.Action button4Action)
	{
		ShowButtonPopup (message, new string[]{ button1Label, button2Label, button3Label, button4Label }, 
			new System.Action[]{ button1Action, button2Action, button3Action, button4Action });
	}

	protected override void Setup ()
	{
		if (popupMenuGO != null)
			popupMenu = popupMenuGO.GetComponent<PopupMenu> ();
		
		if (textPopupPrefab == null || popupsContainer == null || temporaryPopupsContainer == null ||
		    popupMenuGO == null || popupMenu == null || popupBackground == null) {
			Debug.LogError (this + " has null values.");
			return;
		}

		buttonPopupQueue = new Queue<PopupData> ();
	}

	private void RecursivelySetActive (GameObject parent, bool isActive = true)
	{
		foreach (GameObject child in parent.GetAllChildren(true))
			child.SetActive (isActive);
	}

	/// <summary>
	/// Hides all popups, shows the background for exit, and enables this gameObject and all its children.
	/// </summary>
	private void ShowPopupGameObject (GameObject popupGameObject)
	{
		HideAllPopups ();
		popupsContainer.SetActive (true);
		popupBackground.SetActive (true);
		RecursivelySetActive (popupGameObject);
	}

	private void ShowTemporaryPopups ()
	{
		popupsContainer.SetActive (true);
		popupBackground.SetActive (true);
		RecursivelySetActive (temporaryPopupsContainer, true);
	}

	new void Start ()
	{
		base.Start ();

		HideAllPopups ();
	}

	private struct PopupData
	{
		public string message;
		public string[] buttonLabels;
		public System.Action[] buttonActions;

		public PopupData (string message, string[] buttonLabels, System.Action[] buttonActions)
		{
			this.message = message;
			this.buttonLabels = buttonLabels;
			this.buttonActions = buttonActions;
		}
	}
}

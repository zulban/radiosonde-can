﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupImageText : MonoBehaviourWithSetup
{
	[SerializeField]
	protected GameObject imageTextPrefab, innerContainer, container;

	private List<ImageTextUI> imageTextUIs = new List<ImageTextUI> ();

	/// <summary>
	/// The ContentSizeFitter needs one frame to update its height.
	/// This int tracks the frame when AddImageText were called, 
	/// so that ImageTextUI can refresh its height one frame later, 
	/// and this can refresh its height two frames later.
	/// </summary>
	private int frameOfAdds = -1;

	private void DestroyPanels ()
	{
		foreach (var toDestroy in imageTextUIs)
			Destroy (toDestroy.gameObject);		
		imageTextUIs = new List<ImageTextUI> ();
	}

	/// <summary>
	/// You can add many ImageText panels.
	/// It must all be done in one frame - if AddImageText is called on a new frame, all panels are cleared first.
	/// </summary>
	public void AddImageText (ImageTextData data)
	{
		if (frameOfAdds < Time.frameCount)
			DestroyPanels ();
		frameOfAdds = Time.frameCount;

		GameObject newImageText = Instantiate (imageTextPrefab, innerContainer.transform);

		//Start the panel out of sight until a couple frames pass so it can be resizes and positioned properly.
		newImageText.GetComponent<RectTransform> ().position = new Vector2 (innerContainer.GetComponent<RectTransform> ().rect.width * 2, 0);

		ImageTextUI newImageTextUI = newImageText.GetComponent<ImageTextUI> ();
		newImageTextUI.SetImageText (data);
		imageTextUIs.Add (newImageTextUI);
	}

	void Update ()
	{
		if (frameOfAdds > 0 && frameOfAdds == Time.frameCount - 3) {
			RefreshContainerHeightAndPanelY ();
		}
	}

	protected override void Setup ()
	{
		if (imageTextPrefab == null || innerContainer == null || container == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}
	}

	private void RefreshContainerHeightAndPanelY ()
	{
		float verticalPadding = 30;

		float heightTotal = 0;
		foreach (var imageTextUI in imageTextUIs) {
			var rt = imageTextUI.gameObject.GetComponent<RectTransform> ();
			rt.anchoredPosition = new Vector2 (0, -heightTotal - verticalPadding);
			heightTotal += imageTextUI.RT.rect.height;
		}

		float innerHeight = heightTotal - container.GetComponent<RectTransform> ().rect.height;
		innerHeight += verticalPadding * 2;
		var innerRT = innerContainer.GetComponent<RectTransform> ();
		innerRT.sizeDelta = new Vector2 (innerRT.sizeDelta.x, innerHeight);

		//set scrollabr to top
		Scrollbar scrollbar = container.GetComponent<ScrollRect> ().verticalScrollbar;
		scrollbar.value = 1;
	}
}

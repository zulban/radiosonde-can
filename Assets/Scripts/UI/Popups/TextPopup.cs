﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopup : PopupWithButtons
{
	[SerializeField]
	protected Text textCenter, textTop;

	public void SetCenterMessage (string message)
	{
		textCenter.text = message;

		textCenter.enabled = true;
		textTop.enabled = false;
	}

	public void SetTopMessage (string message)
	{
		textTop.text = message;

		textCenter.enabled = false;
		textTop.enabled = true;
	}

	protected override void Setup ()
	{
		base.Setup ();

		if (textCenter == null || textTop == null) {
			Debug.LogError (this + " has null values.");
		}
	}
}

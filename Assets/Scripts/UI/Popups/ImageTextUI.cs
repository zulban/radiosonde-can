﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This sets the UI for one image row (one, two, or three images) and one text in a PopupImageText.
/// </summary>
[RequireComponent (typeof(RectTransform))]
public class ImageTextUI : MonoBehaviourWithSetup
{
	[SerializeField]
	public Image image1, image2, image3;

	[SerializeField]
	public Text titleText, paragraphText;

	private RectTransform rt;
	private Image[] images;

	/// <summary>
	/// The ContentSizeFitter needs one frame to update its height.
	/// This int tracks the frame when setup was called, so that resize can be done on the next frame.
	/// </summary>
	private int frameOfSetup = 0;

	public RectTransform RT {
		get { 
			if (rt == null)
				rt = GetComponent<RectTransform> ();
			return rt;
		}
	}

	public void SetImageText (ImageTextData data)
	{
		SetupIfSetupHasNotRun ();

		titleText.text = data.title;
		paragraphText.text = data.paragraph;

		for (int i = 0; i < 3; i++) {
			bool hasSprite = data.sprites [i] != null;
			images [i].enabled = hasSprite;
			if (hasSprite)
				images [i].sprite = data.sprites [i];			
		}

		frameOfSetup = Time.frameCount;

		// One line descriptions that are short are centered, not left justified.
		bool center = !data.paragraph.Contains ("\n") && data.paragraph.Length < 80;
		paragraphText.alignment = center ? TextAnchor.UpperCenter : TextAnchor.UpperLeft;
	}

	protected override void Setup ()
	{
		images = new Image[]{ image1, image2, image3 };

		if (image1 == null || image2 == null || image3 == null || paragraphText == null || titleText == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}
	}

	void Update ()
	{
		if (frameOfSetup > 0 && frameOfSetup == Time.frameCount - 2) {
			RefreshSize ();
		}
	}

	private void RefreshSize ()
	{		
		float newSize = image1.rectTransform.rect.height + titleText.rectTransform.rect.height + paragraphText.rectTransform.rect.height;
		RT.sizeDelta = new Vector2 (RT.sizeDelta.x, newSize);
		gameObject.SetActive (false);
		gameObject.SetActive (true);
	}
}

/// <summary>
/// All of the data required to set the appearance of an ImageTextUI.
/// Sprite images will be shown centered, in order from left to right.
/// 
/// If just one sprite is provided, it can be much wider than it is tall.
/// </summary>
public class ImageTextData
{
	public string title, paragraph;
	public Sprite[] sprites;

	public ImageTextData (Sprite sprite, string title, string paragraph)
	{
		sprites = new Sprite[]{ null, sprite, null };
		this.title = title;
		this.paragraph = paragraph;
	}

	public ImageTextData (Sprite[] sprites, string title, string paragraph)
	{
		// For one sprite, only the center is not null.
		// For two sprites, the center of the three is null.
		if (sprites.Length == 1)
			this.sprites = new Sprite[]{ null, sprites [0], null };
		else if (sprites.Length == 2)
			this.sprites = new Sprite[]{ sprites [0], null, sprites [1] };
		else if (sprites.Length == 3)
			this.sprites = sprites;
		else
			throw new System.NotImplementedException ();
		
		this.title = title;
		this.paragraph = paragraph;
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupMenu : MonoBehaviourWithSetup
{
	[SerializeField]
	protected Text[] buttonLabels;

	[SerializeField]
	protected Button[] buttons;

	public void SetButtons (string[] labels, System.Action[] clickActions)
	{
		if (labels.Length > buttonLabels.Length) {
			Debug.LogError ("Too many labels.");
			return;
		}
		if (clickActions.Length > buttons.Length) {
			Debug.LogError ("Too many actions.");
			return;
		}
		if (labels.Length != clickActions.Length) {
			Debug.LogError ("labels count must match clickActions count.");
			return;
		}

		for (int i = 0; i < buttons.Length; i++) {
			if (i < labels.Length) {
				buttons [i].gameObject.SetActive (true);

				buttonLabels [i].text = labels [i];
				buttons [i].onClick.RemoveAllListeners ();
				int index = i;
				buttons [i].onClick.AddListener (() => {
					PopupManager.Instance.HideAllPopups ();
					var action = clickActions [index];
					if (action != null)
						action.Invoke ();
				});
			} else {
				buttons [i].gameObject.SetActive (false);
			}
		}
	}

	protected override void Setup ()
	{
		if (buttonLabels == null || buttons == null || buttonLabels.Length != buttons.Length) {
			Debug.LogError (this + " not setup.");
			return;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PopupWithButtons : MonoBehaviourWithSetup
{
	[SerializeField]
	protected Button button1of1, button1of2, button2of2, button1of3, button2of3, button3of3;

	[SerializeField]
	protected Button[] buttons6;

	private Button[] buttons;

	public void SetButtons (string[] buttonLabels, System.Action[] buttonActions)
	{
		if (buttonLabels.Length != buttonActions.Length) {
			Debug.LogError ("buttonLabels size must match buttonActions size.");
			return;
		}

		HideButtons ();

		// Figure out the indexes we will use from the buttons array
		int[] indexes;
		int count = buttonLabels.Length;
		if (count == 1)
			indexes = new int[]{ 0 };
		else if (count == 2)
			indexes = new int[]{ 1, 2 };
		else if (count == 3)
			indexes = new int[]{ 3, 4, 5 };
		else
			indexes = new int[]{ 6, 7, 8, 9, 10, 11 };

		// Set the label and action for buttons
		foreach (int componentIndex in indexes) {

			// map the indexes for buttons onto buttonLabels 
			int argumentIndex = componentIndex - indexes [0];

			// Only SetButton if we have a label and action, for example setting just 5 of 6 buttons.
			if (count > argumentIndex)
				SetButton (buttons [componentIndex], buttonLabels [argumentIndex], buttonActions [argumentIndex]);
		}
		
	}

	public void SetOneButton (string buttonText, System.Action  clickFunction)
	{
		SetButtons (new string[]{ buttonText }, new System.Action[]{ clickFunction });
	}

	public void SetTwoButtons (string leftButtonText, string rightButtonText, System.Action onClickLeft, System.Action  onClickRight)
	{
		SetButtons (new string[]{ leftButtonText, rightButtonText }, new System.Action[]{ onClickLeft, onClickRight });
	}

	public void SetThreeButtons (string button1Text, string button2Text, string button3Text, System.Action onClick1, System.Action  onClick2, System.Action onClick3)
	{
		SetButtons (new string[]{ button1Text, button2Text, button3Text }, new System.Action[] {
			onClick1,
			onClick2,
			onClick3
		});
	}

	public void SetFourButtons (string button1Text, string button2Text, string button3Text, string button4Text, System.Action onClick1, System.Action  onClick2, System.Action onClick3, System.Action onClick4)
	{
		SetButtons (new string[]{ button1Text, button2Text, button3Text, button4Text }, new System.Action[] {
			onClick1,
			onClick2,
			onClick3,
			onClick4
		});
	}

	protected override void Setup ()
	{
		if (buttons6.Length != 6) {
			Debug.LogError (this + " needs 6 buttons in buttons6.");
			return;
		}
		buttons = new Button[] {
			button1of1,
			button1of2,
			button2of2,
			button1of3,
			button2of3,
			button3of3,
			buttons6 [0],
			buttons6 [1],
			buttons6 [2],			
			buttons6 [3],
			buttons6 [4],
			buttons6 [5]
		};
		foreach (var button in buttons) {
			if (button == null) {
				Debug.LogError (this + " has null values.");
				return;
			}
		}
	}

	private void HideButtons ()
	{
		SetupIfSetupHasNotRun ();

		foreach (var button in buttons)
			button.gameObject.SetActive (false);
	}

	private void SetButton (Button button, string buttonText, System.Action clickFunction)
	{
		SetupIfSetupHasNotRun ();

		button.gameObject.SetActive (true);
		button.onClick = new Button.ButtonClickedEvent ();
		button.onClick.AddListener (new UnityEngine.Events.UnityAction (delegate {
			clickFunction ();
		}));
		foreach (var text in ParentChildFunctions.GetComponentsInChildren<Text>(button.gameObject)) {
			text.text = buttonText;
			return;
		}
		Debug.LogError (this + " found no UI.Text for this button.");
	}
}

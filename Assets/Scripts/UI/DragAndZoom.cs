﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Attach this to a GameObject to enable drag and zoom functionality on it.
/// </summary>
public class DragAndZoom : RefreshableUI, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public delegate void DragEvent ();

	public event DragEvent OnEndDragDelegate, OnBeginDragDelegate;

	public void OnBeginDrag (PointerEventData eventData)
	{
		if (OnBeginDragDelegate != null)
			OnBeginDragDelegate ();
	}

	public void OnDrag (PointerEventData eventData)
	{
		
	}

	public virtual void OnEndDrag (PointerEventData eventData)
	{		
		if (OnEndDragDelegate != null)
			OnEndDragDelegate ();
	}

	protected override void Setup ()
	{

	}

	public override void RefreshAppearance ()
	{

	}
}

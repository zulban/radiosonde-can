﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonManager : Singleton<BalloonManager>
{
	[SerializeField]
	protected GameObject balloonPrefab;

	[SerializeField]
	protected GameObject balloonLandParticlePrefab;

	private GameObject balloonContainer;
	private List<Balloon> balloons;

	/// <summary>
	/// How many seconds for each physics tick, to override deltaTime and ensure determinism.
	/// </summary>
	private float determinismIntervalSeconds = 0.02f;
	private float timeSinceLastComputeStep = 0;

	/// <summary>
	/// Clear balloons automatically after this many seconds of inactivity.
	/// </summary>
	private const float autoClearBalloonSeconds = 60;

	protected override void Setup ()
	{
		if (balloonPrefab == null || balloonLandParticlePrefab == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}

		balloons = new List<Balloon> ();
		balloonContainer = new GameObject ("Balloons");
		balloonContainer.transform.parent = Earth.Instance.gameObject.transform;
	}

	public static Quaternion GetBalloonRotation (Vector3 position)
	{
		Vector3 gravityVector = Earth.Instance.transform.position - position;
		Vector3 north = Vector3.Cross (gravityVector, Vector3.right);
		return Quaternion.LookRotation (north, Vector3.up);
	}

	public Balloon PlaceNewBalloon (Vector3 position, int playerID, bool autostartDrift = true)
	{
		NavigationManager.Instance.NotifyActivity ();

		Quaternion startRotation = GetBalloonRotation (position);

		GameObject balloonGO = Instantiate<GameObject> (balloonPrefab, position, startRotation, balloonContainer.transform);
		Balloon balloon = balloonGO.GetComponent<Balloon> ();

		balloon.balloonGameObject.GetComponent<MeshRenderer> ().material.color = PlayManager.Instance.PlayerColor;

		balloons.Add (balloon);

		balloon.SetPlayerID (playerID);
		balloon.SetInflate (InflateUI.Instance.InflateRatio);
		if (autostartDrift)
			balloon.StartDrift ();
		return balloon;
	}

	public void ResetAllBalloons ()
	{
		foreach (var balloon in balloons)
			balloon.SetPositionToStart ();
	}

	public void ReplayAllBalloons ()
	{
		ResetAllBalloons ();
		foreach (var balloon in balloons) {
			balloon.StartDrift ();
		}
	}

	private float GetDistanceFromEarthCenter (Balloon balloon)
	{
		float totalDriftSeconds = GetTotalDriftSeconds (balloon.InflateAmount);
		if (balloon.DriftSeconds > totalDriftSeconds)
			return Constants.EarthRadius;
		
		float h = totalDriftSeconds / 2;
		float h2 = Mathf.Pow (h, 2);
		float distanceFromSurface = -Mathf.Pow (balloon.DriftSeconds - h, 2) + h2;
		distanceFromSurface /= 8;

		return Constants.EarthRadius + distanceFromSurface;
	}

	/// <summary>
	/// Returns the delta position this balloon should move for one tick.
	/// </summary>
	private Vector3 GetTangentVelocity (Balloon balloon)
	{
		Vector3 wind = WindManager.Instance.GetWindVector (balloon.gameObject.transform.position, false);
		return wind / 40;
	}

	private void UpdateBalloonPositions ()
	{
		timeSinceLastComputeStep += Time.deltaTime;
		int stepsToCompute = Mathf.FloorToInt (timeSinceLastComputeStep / determinismIntervalSeconds);

		if (stepsToCompute > 0) {
			foreach (var balloon in balloons) {
				
				if (!balloon.IsDrifting)
					continue;
				
				for (int step = 0; step < stepsToCompute; step++) {
					TickBalloon (balloon);
				}
			}

			timeSinceLastComputeStep -= stepsToCompute * determinismIntervalSeconds;
		}
	}

	private void CreateBalloonLandingParticle (Balloon balloon)
	{
		GameObject particles = Instantiate<GameObject> (balloonLandParticlePrefab, balloon.transform.position, Quaternion.identity, balloon.transform);
		ParticleSystem ps = particles.GetComponent<ParticleSystem> ();

		var psMain = ps.main;
		psMain.startColor = PlayManager.Instance.PlayerColor;

		Destroy (particles, 5);
	}

	private void TickBalloon (Balloon balloon)
	{
		// No position change if drift is over.
		float totalDriftSeconds = GetTotalDriftSeconds (balloon.InflateAmount);
		if (balloon.DriftSeconds > totalDriftSeconds) {
			if (balloon.IsDrifting) {
				CreateBalloonLandingParticle (balloon);
				PlayManager.Instance.NotifyBalloonLanding (balloon);
				balloon.StopDrift ();
			}
			return;	
		}
		
		Vector3 tangentVelocity = GetTangentVelocity (balloon);
		float distanceFromEarthCenter = GetDistanceFromEarthCenter (balloon);
		// combine wind with previous velocity
		float ratio = 0.03f + balloon.InflateAmount * 0.03f;
		tangentVelocity = Vector3.Lerp (balloon.previousTangentVelocity, tangentVelocity, ratio);
		balloon.previousTangentVelocity = tangentVelocity;

		Vector3 newPosition = balloon.Position + tangentVelocity;
		float heightRatioAdjust = distanceFromEarthCenter / newPosition.magnitude;
		newPosition = newPosition * heightRatioAdjust;

		balloon.transform.position = newPosition;
		balloon.AddToDriftSeconds (timeSinceLastComputeStep);
	}

	public void ClearBalloons ()
	{
		foreach (var balloon in balloons) {
			Destroy (balloon.gameObject);
		}
		balloons = new List<Balloon> ();
	}

	private float GetTotalDriftSeconds (float inflateAmount)
	{
		return 4 + 8 * inflateAmount;
	}

	void Update ()
	{
		UpdateBalloonPositions ();

		if (balloons.Count > 0 && NavigationManager.Instance.SecondsSinceLastActivity > autoClearBalloonSeconds) {
			ClearBalloons ();
			PlayManager.Instance.NewGame ();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviourWithSetup
{
	[SerializeField]
	public GameObject balloonGameObject;

	private int playerID;

	public int PlayerID{ get { return playerID; } }

	private bool isDrifting = false;
	private Vector3 startPosition;
	private Quaternion startRotation;
	private float inflateAmount;
	private float driftSeconds;

	/// <summary>
	/// The most recent wind velocity applied to the balloon. This is tangent to the Earth.
	/// </summary>
	public Vector3 previousTangentVelocity;

	public Vector3 Position{ get { return transform.position; } }

	public bool IsDrifting{ get { return isDrifting; } }

	public float DriftSeconds{ get { return driftSeconds; } }

	public float InflateAmount{ get { return inflateAmount; } }

	public Vector3 StartPosition{ get { return startPosition; } }

	/// <summary>
	/// This value is cached each frame.
	/// </summary>
	private Vector3 gravityUnitVector;
	private int lastGravityVectorFrame;

	public Vector3 GravityUnitVector {
		get { 
			if (Time.frameCount != lastGravityVectorFrame) {
				lastGravityVectorFrame = Time.frameCount;
				gravityUnitVector = (Earth.Instance.transform.position - Position).normalized;
			}				
			return gravityUnitVector;
		}
	}

	protected override void Setup ()
	{
		if (balloonGameObject == null) {
			Debug.LogError (this + " has nulls.");
			return;
		}
		startPosition = gameObject.transform.position;
		startRotation = gameObject.transform.rotation;
	}

	public void StartDrift ()
	{
		SetupIfSetupHasNotRun ();

		driftSeconds = 0;
		isDrifting = true;
	}

	public void StopDrift ()
	{
		SetupIfSetupHasNotRun ();

		isDrifting = false;
	}

	public void AddToDriftSeconds (float seconds)
	{
		SetupIfSetupHasNotRun ();

		driftSeconds += seconds;
	}

	public void SetPositionToStart ()
	{
		SetupIfSetupHasNotRun ();

		transform.position = startPosition;
		driftSeconds = 0;
	}

	public void SetPlayerID (int playerID)
	{
		SetupIfSetupHasNotRun ();

		this.playerID = playerID;
	}

	public void SetInflate (float inflateAmount)
	{
		SetupIfSetupHasNotRun ();

		inflateAmount = Mathf.Clamp (inflateAmount, 0, 1);
		this.inflateAmount = inflateAmount;
		float scale = 0.5f + inflateAmount / 2;
		balloonGameObject.transform.localScale = new Vector3 (scale, scale, scale);
	}

	void OnDrawGizmos ()
	{
		Vector3 p = gameObject.transform.position;
		Vector3 wind = WindManager.Instance.GetWindVector (p, false);
		wind *= 5;
		Vector3 target = p + wind;
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (p, target);
		Gizmos.DrawSphere (target, 0.3f);
	}
}


VERSION=$1
PACKAGE=com.FrameOfMindGames.RadiosondeCAN
DEVICE=`./get_adb_device_id.py`

if [ -z "$VERSION" ] ; then
	APK=Builds/RadiosondeCAN.apk
else
	APK=Builds/RadiosondeCAN-${VERSION}.apk
fi

set -eu

if [ ! -f $APK ] ; then
	echo "Not a file: '$APK'"
	exit 1
fi
echo "Using APK: '$APK'"

if [ -z "$DEVICE" ]; then
	echo "Failed to find adb DEVICE."
	exit 1
fi

set -ex

adb -s $DEVICE uninstall $PACKAGE
adb -s $DEVICE install $APK
adb shell monkey -p $PACKAGE -c android.intent.category.LAUNCHER 1

#!/usr/bin/python3

"Prints the adb device ID by parsing 'adb devices'. Exits 1 if cannot parse."

import subprocess, sys, re

cmd="adb devices 2>/dev/null"
output=subprocess.check_output(cmd,shell=True)
if not output:
    sys.exit(1)

output=output.decode("utf8").strip()

text="List of devices attached"
if not output.startswith(text):
    sys.exit(1)

if not output.replace(text,"").strip():
    sys.exit(1)

lines=output.strip().split("\n")
device="unknown"
for line in lines:
    if line.endswith("device"):
        device=re.split("[ \t\n]+",line)[0]

print(device)

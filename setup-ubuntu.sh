#https://forum.unity3d.com/threads/unity-on-linux-release-notes-and-known-issues.350256/

#wget http://beta.unity3d.com/download/ee86734cf592/unity-editor_amd64-2017.2.0f3.deb

sudo apt autoremove
sudo apt install libgtk2.0-0 libsoup2.4-1 libarchive13 libpng-dev

sudo dpkg -i unity-editor_amd64-2017.2.0f3.deb
sudo apt-get install -f
sudo dpkg -i unity-editor_amd64-2017.2.0f3.deb

sudo apt-get install monodevelop mono-reference-assemblies-*

# TO MAKE AN APK
# Download Android SDK, JDK, and NDK.

# download Android Studio to get "android-sdk" folder.
#   https://developer.android.com/studio/

# Latest version of Android SDK has some issue... replace tools folder:
#   https://answers.unity.com/questions/1323731/unable-to-list-target-platforms-please-make-sure-t.html
#   http://dl-ssl.google.com/android/repository/tools_r25.2.5-windows.zip

# Java JDK
sudo apt install openjdk-8-jdk openjdk-9-jdk

# Download NDK r13b
# https://developer.android.com/ndk/downloads/older_releases

# Finally, set Unity Edit > Preferences > External Tools, for all three.

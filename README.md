This simple game was developed in a few days by the Meteorological Service of Canada (MSC) at Environment and Climate Change Canada (ECCC). It uses real wind data from [Datamart](https://dd.weather.gc.ca/), MSC's open data service. The app is available on the [Google Play store for free](https://play.google.com/store/apps/details?id=com.FrameOfMindGames.RadiosondeCAN).

To build the game, the game making tool Unity3D was used along with the programming language C#. If you have any questions about how to make 3D games, or how to use the MSC open data, feel free to contact Stuart Spence:

stuart.spence@canada.ca

# Screenshots

![screenshot](Promotional/Screenshots/readme-screenshot.png)

# About this repository

If you have some experience with coding, you may notice several extra files and methods in this repository. That's because I copied a lot of code from another project of mine, [ChessCraft](https://www.chesscraft.ca), which served as a template to get started on RadiosondeCAN.
